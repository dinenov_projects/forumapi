import { Test, TestingModule } from '@nestjs/testing';
import { AdminService } from './admin.service';
import { UsersService } from '../core/services/users.service';

describe('AdminService', () => {
  let service: AdminService;

  const testUser = {
    id: '1',
    username: 'username',
  };

  const mockUserService = {
    deleteUser: (userId: string) => {
      if (userId === testUser.id) {
        return testUser;
      } else {
        return undefined;
      }
    },
    banUser: (userId: string, description: string) => {
      if (userId === testUser.id) {
        return testUser;
      } else {
        return undefined;
      }
    },
    unbanUser: (userId: string) => {
      if (userId === testUser.id) {
        return testUser;
      } else {
        return undefined;
      }
    },
    getUserById: (userId: string) => {
      if (userId === testUser.id) {
        return testUser;
      } else {
        return undefined;
      }
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AdminService, {
        provide: UsersService,
        useValue: mockUserService,
      }],
    }).compile();

    service = module.get<AdminService>(AdminService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('deleteUser', () => {
    it('deleteUser should return the expected result', async () => {
      const deletedUser = await service.deleteUser('1');
      expect(deletedUser).toEqual(testUser);
    });

    it('deleteUser should call userService.deleteUser', async () => {
      const deleteUser = jest.spyOn(mockUserService, 'deleteUser');
      await service.deleteUser('1');
      expect(deleteUser).toBeCalledTimes(1);
      deleteUser.mockRestore();
    });

    it('deleteUser should return undefined, when user does not exist in the database', async () => {
      const result = await service.deleteUser('0');
      expect(result).toBeUndefined();
    });
  });

  describe('banUser', () => {
    it('banUser should return the expected result', async () => {
      const deletedUser = await service.banUser('1', 'description');
      expect(deletedUser).toEqual(testUser);
    });

    it('banUser should call userService.banUser', async () => {
      const banUser = jest.spyOn(mockUserService, 'banUser');
      await service.banUser('1', 'description');
      expect(banUser).toBeCalledTimes(1);
      banUser.mockRestore();
    });

    it('banUser should return undefined, when user does not exist in the database', async () => {
      const result = await service.banUser('0', 'description');
      expect(result).toBeUndefined();
    });
  });

  describe('unbanUser', () => {
    it('unbanUser should return the expected result', async () => {
      const deletedUser = await service.unbanUser('1');
      expect(deletedUser).toEqual(testUser);
    });

    it('unbanUser should call userService.unbanUser', async () => {
      const unbanUser = jest.spyOn(mockUserService, 'unbanUser');
      await service.unbanUser('1');
      expect(unbanUser).toBeCalledTimes(1);
      unbanUser.mockRestore();
    });

    it('unbanUser should return undefined, when user does not exist in the database', async () => {
      const result = await service.unbanUser('0');
      expect(result).toBeUndefined();
    });
  });

  describe('findUserById', () => {
    it('findUserById should return the expected result', async () => {
      const deletedUser = await service.findUserById('1');
      expect(deletedUser).toEqual(testUser);
    });

    it('findUserById should call userService.getUserById', async () => {
      const getUserById = jest.spyOn(mockUserService, 'getUserById');
      await service.findUserById('1');
      expect(getUserById).toBeCalledTimes(1);
      getUserById.mockRestore();
    });

    it('findUserById should return undefined, when user does not exist in the database', async () => {
      const result = await service.findUserById('0');
      expect(result).toBeUndefined();
    });
  });
});
