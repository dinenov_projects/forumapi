import { Test, TestingModule } from '@nestjs/testing';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { BadRequestException } from '@nestjs/common';
import { BanstatusCreateDTO } from '../models/banstatus/banstatus-create.dto';

describe('Admin Controller', () => {
  let controller: AdminController;
  const testUser = {
    id: '1',
    username: 'username',
  };
  const mockAdminService: any = {
    deleteUser: (userId: string) => testUser,
    banUser: (userId: string, banStatus: BanstatusCreateDTO) => testUser,
    unbanUser: (userId: string) => testUser,
    findUserById: (userId: string) => {
      if (testUser.id === userId) {
        return testUser.username;
      } else {
        return null;
      }
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdminController],
      providers: [{
        provide: AdminService,
        useValue: mockAdminService,
      }],
    }).compile();

    controller = module.get<AdminController>(AdminController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('deleteUser', () => {
    it('deleteUser should return correct values', async () => {
      const result = await controller.deleteUser('1');
      expect(result).toEqual({ data: { id: '1', username: 'username' }, message: 'User successfully deleted!' });
    });

    it('deleteUser should call findUserById', async () => {
      const findUseById = jest.spyOn(mockAdminService, 'findUserById');
      await controller.deleteUser('1');
      expect(findUseById).toBeCalledTimes(1);
      findUseById.mockRestore();
    });

    it('deleteUser should call adminService.deleteUser', async () => {
      const deleteUser = jest.spyOn(mockAdminService, 'deleteUser');
      await controller.deleteUser('1');
      expect(deleteUser).toBeCalledTimes(1);
      deleteUser.mockRestore();
    });

    it('deleteUser should throw, when user does not exist in the database', async () => {
      try {
        await controller.deleteUser('0');
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
      }
    });

    it('deleteUser should not call adminService.deleteUser, when user does not exist in the database', async () => {
      const deleteUser = jest.spyOn(mockAdminService, 'deleteUser');
      try {
        await controller.deleteUser('0');
      } catch (err) {
        expect(deleteUser).toBeCalledTimes(0);
      }
      deleteUser.mockRestore();
    });
  });

  describe('banUser', () => {
    const banStatus: BanstatusCreateDTO = {
      description: 'description',
    };

    it('banUser should return correct values', async () => {
      const result = await controller.banUser('1', banStatus);
      expect(result).toEqual({ data: { id: '1', username: 'username' }, message: 'User successfully banned!' });
    });

    it('banUser should call findUserById', async () => {
      const findUseById = jest.spyOn(mockAdminService, 'findUserById');
      await controller.banUser('1', banStatus);
      expect(findUseById).toBeCalledTimes(1);
      findUseById.mockRestore();
    });

    it('banUser should call adminService.banUser', async () => {
      const banUser = jest.spyOn(mockAdminService, 'banUser');
      await controller.banUser('1', banStatus);
      expect(banUser).toBeCalledTimes(1);
      banUser.mockRestore();
    });

    it('banUser should throw, when user does not exist in the database', async () => {
      try {
        await controller.banUser('0', banStatus);
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
      }
    });

    it('banUser should not call adminService.banUser, when user does not exist in the database', async () => {
      const banUser = jest.spyOn(mockAdminService, 'banUser');
      try {
        await controller.deleteUser('0');
      } catch (err) {
        expect(banUser).toBeCalledTimes(0);
      }
      banUser.mockRestore();
    });
  });

  describe('unbanUser', () => {
    it('unbanUser should return correct values', async () => {
      const result = await controller.unbanUser('1');
      expect(result).toEqual({ data: { id: '1', username: 'username' }, message: 'User successfully unbanned!' });
    });

    it('unbanUser should call findUserById', async () => {
      const findUseById = jest.spyOn(mockAdminService, 'findUserById');
      await controller.unbanUser('1');
      expect(findUseById).toBeCalledTimes(1);
      findUseById.mockRestore();
    });

    it('unbanUser should call adminService.unbanUser', async () => {
      const unbanUser = jest.spyOn(mockAdminService, 'unbanUser');
      await controller.unbanUser('1');
      expect(unbanUser).toBeCalledTimes(1);
      unbanUser.mockRestore();
    });

    it('unbanUser should throw, when user does not exist in the database', async () => {
      try {
        await controller.unbanUser('0');
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
      }
    });

    it('unbanUser should not call adminService.unbanUser, when user does not exist in the database', async () => {
      const unbanUser = jest.spyOn(mockAdminService, 'unbanUser');
      try {
        await controller.unbanUser('0');
      } catch (err) {
        expect(unbanUser).toBeCalledTimes(0);
      }
      unbanUser.mockRestore();
    });
  });
});
