import { Test, TestingModule } from '@nestjs/testing';
import { PostsService } from './posts.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../data/entities/user.entity';
import { Post } from '../data/entities/post.entity';
import { FlagPostEntity } from '../data/entities/flag.entity';
import { CommentsService } from '../comments/comments.service';
import { PostShowWithoutCommentsDTO } from '../models/posts/post-show-without-comments.dto';
import { UserShowDTO } from 'src/models/user';
import { PostShowWithCommentsDTO } from 'src/models/posts/post-show-with-comments.dto';
import { ShowCommentDTO } from 'src/models/comments/show-comment.dto';

describe('PostsService', () => {
  let service: PostsService;

  const like: UserShowDTO = {
    banstatus: undefined,
    id: undefined,
    roles: undefined,
    username: undefined,
  };

  const author: UserShowDTO = {
    banstatus: undefined,
    id: undefined,
    roles: undefined,
    username: undefined,
  };

  const post: any = {
    author: {
      username: undefined,
    },
    like: [like],
    dislike: [like],
    comments: [{}],
  };

  const flag: any = {
    postId: post,
    userId: author,
  };

  const plainToClassPost: PostShowWithoutCommentsDTO = {
    author,
    body: undefined,
    dateModified: undefined,
    datePosted: undefined,
    description: undefined,
    dislike: [like],
    flag: undefined,
    id: undefined,
    isLocked: undefined,
    like: [like],
    title: undefined,
  };

  const comment: ShowCommentDTO = {
    author: undefined,
    body: undefined,
    dateCreated: undefined,
    dateModified: undefined,
    id: undefined,
  };

  const plainToClassPostWithComments: PostShowWithCommentsDTO = {
    author,
    body: undefined,
    dateModified: undefined,
    datePosted: undefined,
    description: undefined,
    dislike: [like],
    flag: undefined,
    id: undefined,
    isLocked: undefined,
    like: [like],
    title: undefined,
    comments: [comment],
  };

  const mockUsersRepository = {
    findOne: (options: any) => author,
  };

  const mockPostsRepository = {
    findOne: (options: any) => post,
    create: (options: any) => post,
    save: (options: any) => post,
    find: (options: any) => [post],
  };

  const mockFlagPostRepository = {
    create: (options: any) => flag,
    find: (options: any) => [flag],
    save: (options: any) => flag,
  };

  const mockCommentsService = {
    createCommentDTO: (comments: any) => comments,
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PostsService, {
        provide: getRepositoryToken(User),
        useValue: mockUsersRepository,
      }, {
          provide: getRepositoryToken(Post),
          useValue: mockPostsRepository,
        }, {
          provide: getRepositoryToken(FlagPostEntity),
          useValue: mockFlagPostRepository,
        }, {
          provide: CommentsService,
          useValue: mockCommentsService,
        }],
    }).compile();

    service = module.get<PostsService>(PostsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('isUserCreator', () => {
    it('isUserCreator should return true if the user is creator', async () => {
      const user: any = { username: undefined };
      const result = await service.isUserCreator(user, 'postId');
      expect(result).toBe(true);
    });

    it('isUserCreator should return false if the user is not creator', async () => {
      const user: any = { username: 'NotUser' };
      const result = await service.isUserCreator(user, 'postId');
      expect(result).toBe(false);
    });

    it('isUserCreator should call postRepository.findOne', async () => {
      const findOne = jest.spyOn(mockPostsRepository, 'findOne');
      const user: any = { username: 'NotUser' };
      await service.isUserCreator(user, 'postId');
      expect(findOne).toBeCalledTimes(1);
    });
  });

  describe('getAllPosts', () => {
    it('getAllPosts should return correct value', async () => {
      const result = await service.getAllPosts(0);
      expect(result).toEqual([plainToClassPost]);
    });

    it('getAllPosts should return correct value', async () => {
      const result = await service.getAllPosts('0');
      expect(result).toEqual([]);
    });

    it('getAllPosts should call postRepository.find', async () => {
      const find = jest.spyOn(mockPostsRepository, 'find');
      await service.getAllPosts('0');
      expect(find).toBeCalledTimes(1);
      find.mockRestore();
    });

    it('getAllPosts should call createPostShowDTO', async () => {
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO');
      await service.getAllPosts('0');
      expect(createPostShowDTO).toBeCalledTimes(1);
      createPostShowDTO.mockRestore();
    });
  });

  describe('createPostShowDTO', () => {
    it('createPostShowDTO should return correct values', async () => {
      const result = await service.createPostShowDTO(post);
      expect(result).toEqual(plainToClassPostWithComments);
    });
  });

  describe('getPostById', () => {
    it('getPostById should return correct values', async () => {
      const result = await service.getPostById('postId');
      expect(result).toEqual(plainToClassPostWithComments);
    });

    it('getPostById should return undefined', async () => {
      const findOne = jest.spyOn(mockPostsRepository, 'findOne').mockImplementation(() => Promise.resolve(undefined));
      const result = await service.getPostById('postId');
      expect(result).toEqual(undefined);
      findOne.mockRestore();
    });

    it('getPostById should call postRepository.findOne', async () => {
      const findOne = jest.spyOn(mockPostsRepository, 'findOne');
      await service.getPostById('postId');
      expect(findOne).toBeCalledTimes(1);
      findOne.mockRestore();
    });

    it('getPostById should call createPostDTO', async () => {
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO');
      await service.getPostById('postId');
      expect(createPostShowDTO).toBeCalledTimes(1);
      createPostShowDTO.mockRestore();
    });
  });

  describe('create', () => {
    it('create should return correct values', async () => {
      const result = await service.create(post, author);
      expect(result).toEqual(plainToClassPost);
    });

    it('create should call postRepository.create', async () => {
      const create = jest.spyOn(mockPostsRepository, 'create');
      await service.create(post, author);
      expect(create).toBeCalledTimes(1);
      create.mockRestore();
    });

    it('create should call usersRepository.findOne', async () => {
      const findOne = jest.spyOn(mockUsersRepository, 'findOne');
      await service.create(post, author);
      expect(findOne).toBeCalledTimes(1);
      findOne.mockRestore();
    });

    it('create should call postRepository.save', async () => {
      const save = jest.spyOn(mockPostsRepository, 'save');
      await service.create(post, author);
      expect(save).toBeCalledTimes(1);
      save.mockRestore();
    });

    it('create should call createPostShowDTO', async () => {
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO');
      await service.create(post, author);
      expect(createPostShowDTO).toBeCalledTimes(1);
      createPostShowDTO.mockRestore();
    });
  });

  describe('updatePost', () => {
    it('updatePost should return correct values', async () => {
      const result = await service.updatePost('postId', plainToClassPost);
      expect(result).toEqual(plainToClassPost);
    });

    it('updatePost should return undefined', async () => {
      const findOne = jest.spyOn(mockPostsRepository, 'findOne').mockImplementation(() => Promise.resolve(undefined));
      const result = await service.updatePost('postId', plainToClassPost);
      expect(result).toEqual(undefined);
      findOne.mockRestore();
    });

    it('updatePost should call postsRepository.findOne', async () => {
      const findOne = jest.spyOn(mockPostsRepository, 'findOne');
      await service.updatePost('postId', plainToClassPost);
      expect(findOne).toBeCalledTimes(1);
      findOne.mockRestore();
    });

    it('updatePost should call postsRepository.save', async () => {
      const save = jest.spyOn(mockPostsRepository, 'save');
      await service.updatePost('postId', plainToClassPost);
      expect(save).toBeCalledTimes(1);
      save.mockRestore();
    });

    it('updatePost should call createPostShowDTO', async () => {
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO');
      await service.updatePost('postId', plainToClassPost);
      expect(createPostShowDTO).toBeCalledTimes(1);
      createPostShowDTO.mockRestore();
    });
  });

  describe('deletePost', () => {
    it('deletePost should return correct values', async () => {
      const result = await service.deletePost('postId', author);
      expect(result).toEqual(plainToClassPost);
    });

    it('deletePost should return correct values when not authenticated', async () => {
      const user: any = { username: 'NotUser' };
      const result = await service.deletePost('postId', user);
      expect(result).toEqual(undefined);
    });

    it('deletePost should call postsRepository.findOne', async () => {
      const findOne = jest.spyOn(mockPostsRepository, 'findOne');
      await service.deletePost('postId', author);
      expect(findOne).toBeCalledTimes(1);
      findOne.mockRestore();
    });
  });

  describe('filterByUser', () => {
    it('filterByUser should return correct values', async () => {
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO').mockImplementation((mockPost: any) => Promise.resolve(post));
      const authorAny: any = {
        posts: [{}],
      };
      const findOne = jest.spyOn(mockUsersRepository, 'findOne').mockImplementation((userId: string) => authorAny);
      const result = await service.filterByUser(undefined);
      expect(result).toEqual([plainToClassPost]);
      createPostShowDTO.mockRestore();
      findOne.mockRestore();
    });
  });

  describe('likePost', () => {
    it('likePost should return correct values', async () => {
      const mockPost = {
        like: [],
        dislike: [],
      };
      const mockUser: any = {
        username: 'username',
      };
      const foundOne = jest.spyOn(mockPostsRepository, 'findOne').mockImplementation(() => mockPost);
      const foundUser = jest.spyOn(mockUsersRepository, 'findOne').mockImplementation(() => mockUser);
      const expectedResult = plainToClassPost;
      expectedResult.like = [like];
      const result = await service.likePost(mockUser, 'postId');
      expect(result).toEqual(expectedResult);
      foundOne.mockRestore();
      foundUser.mockRestore();
    });

    it('likePost should return correct values', async () => {
      const mockUser: any = {
        username: 'username',
      };
      const mockPost = {
        like: [mockUser],
        dislike: [],
      };
      const foundOne = jest.spyOn(mockPostsRepository, 'findOne').mockImplementation(() => mockPost);
      const foundUser = jest.spyOn(mockUsersRepository, 'findOne').mockImplementation(() => mockUser);
      const expectedResult = plainToClassPost;
      expectedResult.like = [like];
      const result = await service.likePost(mockUser, 'postId');
      expect(result).toEqual(expectedResult);
      foundOne.mockRestore();
      foundUser.mockRestore();
    });

    it('likePost should return correct values', async () => {
      const mockUser: any = {
        username: 'username',
      };
      const mockPost = {
        like: [],
        dislike: [mockUser],
      };
      const foundOne = jest.spyOn(mockPostsRepository, 'findOne').mockImplementation(() => mockPost);
      const foundUser = jest.spyOn(mockUsersRepository, 'findOne').mockImplementation(() => mockUser);
      const expectedResult = plainToClassPost;
      expectedResult.like = [like];
      const result = await service.likePost(mockUser, 'postId');
      expect(result).toEqual(expectedResult);
      foundOne.mockRestore();
      foundUser.mockRestore();
    });

    it('likePost should return undefined', async () => {
      const mockPost = {
        like: [],
        dislike: [],
      };
      const mockUser: any = {
        username: 'username',
      };
      const foundOne = jest.spyOn(mockPostsRepository, 'findOne').mockImplementation(() => undefined);
      const foundUser = jest.spyOn(mockUsersRepository, 'findOne').mockImplementation(() => mockUser);
      const expectedResult = plainToClassPost;
      expectedResult.like = [like];
      const result = await service.likePost(mockUser, 'postId');
      expect(result).toEqual(undefined);
      foundOne.mockRestore();
      foundUser.mockRestore();
    });
  });

  describe('dislikePost', () => {
    it('dislikePost should return correct values', async () => {
      const mockPost = {
        like: [],
        dislike: [],
      };
      const mockUser: any = {
        username: 'username',
      };
      const foundOne = jest.spyOn(mockPostsRepository, 'findOne').mockImplementation(() => mockPost);
      const foundUser = jest.spyOn(mockUsersRepository, 'findOne').mockImplementation(() => mockUser);
      const expectedResult = plainToClassPost;
      expectedResult.like = [like];
      const result = await service.dislikePost(mockUser, 'postId');
      expect(result).toEqual(expectedResult);
      foundOne.mockRestore();
      foundUser.mockRestore();
    });

    it('dislikePost should return correct values', async () => {
      const mockUser: any = {
        username: 'username',
      };
      const mockPost = {
        like: [mockUser],
        dislike: [],
      };
      const foundOne = jest.spyOn(mockPostsRepository, 'findOne').mockImplementation(() => mockPost);
      const foundUser = jest.spyOn(mockUsersRepository, 'findOne').mockImplementation(() => mockUser);
      const expectedResult = plainToClassPost;
      expectedResult.like = [like];
      const result = await service.dislikePost(mockUser, 'postId');
      expect(result).toEqual(expectedResult);
      foundOne.mockRestore();
      foundUser.mockRestore();
    });

    it('dislikePost should return correct values', async () => {
      const mockUser: any = {
        username: 'username',
      };
      const mockPost = {
        like: [],
        dislike: [mockUser],
      };
      const foundOne = jest.spyOn(mockPostsRepository, 'findOne').mockImplementation(() => mockPost);
      const foundUser = jest.spyOn(mockUsersRepository, 'findOne').mockImplementation(() => mockUser);
      const expectedResult = plainToClassPost;
      expectedResult.like = [like];
      const result = await service.dislikePost(mockUser, 'postId');
      expect(result).toEqual(expectedResult);
      foundOne.mockRestore();
      foundUser.mockRestore();
    });

    it('dislikePost should return undefined', async () => {
      const mockUser: any = {
        username: 'username',
      };
      const mockPost = {
        like: [],
        dislike: [mockUser],
      };
      const foundOne = jest.spyOn(mockPostsRepository, 'findOne').mockImplementation(() => undefined);
      const foundUser = jest.spyOn(mockUsersRepository, 'findOne').mockImplementation(() => mockUser);
      const expectedResult = plainToClassPost;
      expectedResult.like = [like];
      const result = await service.dislikePost(mockUser, 'postId');
      expect(result).toEqual(undefined);
      foundOne.mockRestore();
      foundUser.mockRestore();
    });
  });

  describe('lockPost', () => {
    it('lockPost should return correct values', async () => {
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO').mockImplementation((mockPost: any) => Promise.resolve(post));
      const result = await service.lockPost('postId');
      const expectedResult = plainToClassPost;
      expectedResult.isLocked = true;
      expect(result).toEqual(expectedResult);
      createPostShowDTO.mockRestore();
    });

    it('lockPost should return undefined', async () => {
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO').mockImplementation((mockPost: any) => Promise.resolve(post));
      const findOne = jest.spyOn(mockPostsRepository, 'findOne').mockImplementation(() => Promise.resolve(undefined));
      const result = await service.lockPost('postId');
      const expectedResult = plainToClassPost;
      expectedResult.isLocked = true;
      expect(result).toEqual(undefined);
      createPostShowDTO.mockRestore();
      findOne.mockRestore();
    });
  });

  describe('filterBySort', () => {
    it('filterBySort should return correct values without query', async () => {
      const result = await service.filterBySort({key: 'value'});
      expect(result).toBe(undefined);
    });

    it('filterBySort should return correct values with author query', async () => {
      const post1: any = {
        author: {
          username: 'a',
        },
        like: [like],
        dislike: [like],
        comments: [{}],
        datePosted: 0,
      };
      const post2: any = {
        author: {
          username: 'b',
        },
        like: [like],
        dislike: [like],
        comments: [{}],
        datePosted: 1,
      };
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO').mockImplementation((mockPost: any) => Promise.resolve(post));
      const find = jest.spyOn(mockPostsRepository, 'find').mockImplementation(() => [post1, post2]);
      const result = await service.filterBySort({author: 'value'});
      const expectedResult = plainToClassPost;
      expect(result).toEqual([expectedResult, expectedResult]);
      createPostShowDTO.mockRestore();
      find.mockRestore();
    });

    it('filterBySort should return correct values with author query', async () => {
      const post1: any = {
        author: {
          username: 'b',
        },
        like: [like],
        dislike: [like],
        comments: [{}],
        datePosted: 0,
      };
      const post2: any = {
        author: {
          username: 'a',
        },
        like: [like],
        dislike: [like],
        comments: [{}],
        datePosted: 1,
      };
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO').mockImplementation((mockPost: any) => Promise.resolve(post));
      const find = jest.spyOn(mockPostsRepository, 'find').mockImplementation(() => [post1, post2]);
      const result = await service.filterBySort({author: 'value'});
      const expectedResult = plainToClassPost;
      expect(result).toEqual([expectedResult, expectedResult]);
      createPostShowDTO.mockRestore();
      find.mockRestore();
    });

    it('filterBySort should return correct values with author query', async () => {
      const post1: any = {
        author: {
          username: 'a',
        },
        like: [like],
        dislike: [like],
        comments: [{}],
        datePosted: 0,
      };
      const post2: any = {
        author: {
          username: 'a',
        },
        like: [like],
        dislike: [like],
        comments: [{}],
        datePosted: 1,
      };
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO').mockImplementation((mockPost: any) => Promise.resolve(post));
      const find = jest.spyOn(mockPostsRepository, 'find').mockImplementation(() => [post1, post2]);
      const result = await service.filterBySort({author: 'value'});
      const expectedResult = plainToClassPost;
      expect(result).toEqual([expectedResult, expectedResult]);
      createPostShowDTO.mockRestore();
      find.mockRestore();
    });

    it('filterBySort should return correct values with date query', async () => {
      const post1: any = {
        author: {
          username: 'a',
        },
        like: [like],
        dislike: [like],
        comments: [{}],
        datePosted: 0,
      };
      const post2: any = {
        author: {
          username: 'b',
        },
        like: [like],
        dislike: [like],
        comments: [{}],
        datePosted: 1,
      };
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO').mockImplementation((mockPost: any) => Promise.resolve(post));
      const find = jest.spyOn(mockPostsRepository, 'find').mockImplementation(() => [post1, post2]);
      const result = await service.filterBySort({date: 'value'});
      const expectedResult = plainToClassPost;
      expect(result).toEqual([expectedResult, expectedResult]);
      createPostShowDTO.mockRestore();
      find.mockRestore();
    });

    it('filterBySort should return correct values with date query', async () => {
      const post1: any = {
        author: {
          username: 'a',
        },
        like: [like],
        dislike: [like],
        comments: [{}],
        datePosted: 1,
      };
      const post2: any = {
        author: {
          username: 'b',
        },
        like: [like],
        dislike: [like],
        comments: [{}],
        datePosted: 0,
      };
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO').mockImplementation((mockPost: any) => Promise.resolve(post));
      const find = jest.spyOn(mockPostsRepository, 'find').mockImplementation(() => [post1, post2]);
      const result = await service.filterBySort({date: 'value'});
      const expectedResult = plainToClassPost;
      expect(result).toEqual([expectedResult, expectedResult]);
      createPostShowDTO.mockRestore();
      find.mockRestore();
    });

    it('filterBySort should return correct values with date query', async () => {
      const post1: any = {
        author: {
          username: 'a',
        },
        like: [like],
        dislike: [like],
        comments: [{}],
        datePosted: 0,
      };
      const post2: any = {
        author: {
          username: 'b',
        },
        like: [like],
        dislike: [like],
        comments: [{}],
        datePosted: 0,
      };
      const createPostShowDTO = jest.spyOn(service, 'createPostShowDTO').mockImplementation((mockPost: any) => Promise.resolve(post));
      const find = jest.spyOn(mockPostsRepository, 'find').mockImplementation(() => [post1, post2]);
      const result = await service.filterBySort({date: 'value'});
      const expectedResult = plainToClassPost;
      expect(result).toEqual([expectedResult, expectedResult]);
      createPostShowDTO.mockRestore();
      find.mockRestore();
    });
  });

  describe('flagPost', () => {
    it('flagPost should return correct values', async () => {
      const result = await service.flagPost('postId', author, {reason: 'reason'});
      expect(result).toEqual(flag);
    });

    it('flagPost should return undefined when flagging nonexistent post', async () => {
      const findOne = jest.spyOn(mockPostsRepository, 'findOne').mockImplementation(() => Promise.resolve(undefined));
      const result = await service.flagPost('postId', author, {reason: 'reason'});
      expect(result).toEqual(undefined);
      findOne.mockRestore();
    });
  });
});
