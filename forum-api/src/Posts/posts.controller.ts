import {
    Controller,
    Get,
    HttpStatus,
    HttpCode,
    Param,
    Post,
    Body,
    Put,
    Delete,
    UsePipes,
    ValidationPipe,
    Query,
    NotFoundException,
    UseGuards,
    UnauthorizedException,
} from '@nestjs/common';
import { PostsService } from './posts.service';
import { CreatePostDTO } from '../models/posts/create-post.dto';
import { UpdatePostDTO } from '../models/posts/update-post.dto';
import { UsersService } from '../core/services/users.service';
import { FlagPostDTO } from '../models/flags/flag-post.dto';
import { User } from '../common/decorators/user.decorator';
import { UserShowDTO } from '../models/user';
import { JwtAuthGuard } from '../common/guards/jwt-auth.guard';
import { AuthGuard } from '@nestjs/passport';
import { PostShowWithoutCommentsDTO } from '../models/posts/post-show-without-comments.dto';
import { IsAdmin } from '../common/decorators/is-admin.decorator';
import { RolesGuard } from '../common/guards/roles.guard';
import { Roles } from '../common/decorators/roles.decorator';
import { UserRole } from '../common/enums/user-role.enum';
import { BanstatusGuard } from '../common/guards/banstatus.guard';

@Controller()

export class PostsController {
    constructor(
        private postService: PostsService,
        private usersService: UsersService,
    ) { }

    @Get('posts')
    // @UseGuards(AuthGuard('jwt'), JwtAuthGuard)
    @HttpCode(HttpStatus.OK)
    async getPosts(
        @Query() query: any,
    ) {
        return this.postService.getAllPosts(query);
    }

    @Get('users/:userId/posts')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard)
    @HttpCode(HttpStatus.OK)
    async filterByUser(
        @Param('userId') userId: string,
    ) {
        if (!(await this.usersService.getUserById(userId))) {
            throw new NotFoundException('This user does not exist!');
        }
        return await this.postService.filterByUser(userId);
    }

    @Get('users/posts/sort')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard)
    @HttpCode(HttpStatus.OK)
    async filterBySort(
        @Query() query: string,
    ) {
        return await this.postService.filterBySort(query);
    }

    @Post('posts')
    @UsePipes(
        new ValidationPipe({
            transform: true,
            whitelist: true,
        }),
    )
    @HttpCode(HttpStatus.CREATED)
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, BanstatusGuard)
    async create(
        @Body() post1: CreatePostDTO,
        @User() user: UserShowDTO,
    ): Promise<any> {
        const createdPost: PostShowWithoutCommentsDTO =  await this.postService.create(post1, user);
        return {
            message: 'Post has been submitted successfully!',
            data: createdPost,
            };

    }

    @Get('posts/:postId')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard)
    @HttpCode(HttpStatus.OK)
    async getPost(
        @Param('postId') postId: string,
    ) {
        const post = await this.postService.getPostById(postId);
        if (!post) {
            throw new NotFoundException(`Post does not exist or it has been deleted`);
        }

        return post;
    }

    @Put('posts/:postId')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, BanstatusGuard)
    @UsePipes(
        new ValidationPipe({
            transform: true,
            whitelist: true,
        }),
    )
    async updatePost(
        @Param('postId') postId: string,
        @Body() updatedPost: UpdatePostDTO,
        @User() user: UserShowDTO,
        @IsAdmin() isAdmin: boolean,
    ) {
        if (!(await this.postService.isUserCreator(user, postId)) && !isAdmin) {
            throw new UnauthorizedException('This post cannot be updated by the current user!');
        }
        const updatePost = await this.postService.updatePost(postId, updatedPost);
        if (!updatePost) {
            throw new NotFoundException(`Post does not exist or it has been locked`);
        }
        return {
            message: 'Post has been updated successfully!',
            data: updatePost,
        };
    }

    @Delete('posts/:postId')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, BanstatusGuard)
    @HttpCode(HttpStatus.OK)
    async deletePost(
        @Param('postId') postId: string,
        @User() user: UserShowDTO,
        @IsAdmin() isAdmin: boolean,
    ) {
        if (!(await this.postService.isUserCreator(user, postId)) && !isAdmin) {
            throw new UnauthorizedException('This post cannot be deleted by the current user!');
        }
        const postToDel = await this.postService.deletePost(postId, user);
        if (!postToDel) {
            throw new NotFoundException('Post does not exist!');
        }
        return {
                message: 'Post has been deleted successfully!',
                data: postToDel,
        };
    }

    @Put('posts/:postId/lock')
    @Roles(UserRole.Admin)
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, RolesGuard, BanstatusGuard)
    @HttpCode(HttpStatus.OK)
    async lockPost(
        @Param('postId') postId: string,
        @User() user: UserShowDTO,
        @IsAdmin() isAdmin: boolean,
    ) {
        if (!(await this.postService.isUserCreator(user, postId)) && !isAdmin) {
            throw new UnauthorizedException('This post cannot be locked by the current user!');
        }
        const postToLock = await this.postService.lockPost(postId);
        if (postToLock === undefined) {
            throw new NotFoundException('Post does not exist!');
        }
        return {
                message: 'Post has been locked successfully!',
                data: postToLock,
        };
    }

    @Put('posts/:postId/flag')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, BanstatusGuard)
    @HttpCode(HttpStatus.OK)
    async flagPost(
        @Param('postId') postId: string,
        @User() user: UserShowDTO,
        @Body() reason: FlagPostDTO,
    ) {
        const newPost = await this.postService.flagPost(postId, user, reason);
        if (!newPost) {
            throw new NotFoundException('Post does not exist!');
        }
        return {
            message: 'Post has been Flagged successfully!',
            data: await newPost,
        };
    }

    @Put('posts/:postId/likes')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, BanstatusGuard)
    @HttpCode(HttpStatus.OK)
    async likePost(
        @User() user: UserShowDTO,
        @Param('postId') postID: string,
    ) {
        const likedPost = await this.postService.likePost(user, postID);
        if (!likedPost) {
            throw new NotFoundException('Post does not exist!');
        }
        return {
            message: 'Post has been liked successfully!',
            data: likedPost,
        };
    }

    @Put('posts/:postId/dislikes')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, BanstatusGuard)
    @HttpCode(HttpStatus.OK)
    async dislikePost(
        @User() user: UserShowDTO,
        @Param('postId') postID: string,
    ) {
        const dislikedPost = await this.postService.dislikePost(user, postID);
        if (!dislikedPost) {
            throw new NotFoundException('Post does not exist!');
        }
        return {
            message: 'Post has been disliked successfully!',
            data: dislikedPost,
        };
    }
}
