import { Injectable } from '@nestjs/common';
import { CreatePostDTO } from '../models/posts/create-post.dto';
import { UpdatePostDTO } from '../models/posts/update-post.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Post } from '../data/entities/post.entity';
import { Repository } from 'typeorm';
import { User } from '../data/entities/user.entity';
import { UserShowDTO } from '../models/user';
import { FlagPostEntity } from '../data/entities/flag.entity';
import { FlagPostDTO } from '../models/flags/flag-post.dto';
import { PostShowWithoutCommentsDTO } from '../models/posts/post-show-without-comments.dto';
import { plainToClass } from 'class-transformer';
import { PostShowWithCommentsDTO } from '../models/posts/post-show-with-comments.dto';
import { Comments } from '../data/entities/comment.entity';
import { CommentsService } from '../comments/comments.service';

@Injectable()
export class PostsService {
    public constructor(
        @InjectRepository(Post) private readonly postRepository: Repository<Post>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(FlagPostEntity) private readonly flagPostRepository: Repository<FlagPostEntity>,
        private readonly commentsService: CommentsService,
    ) { }

    async isUserCreator(user: UserShowDTO, postId: string): Promise<boolean> {
        const post = await this.postRepository.findOne({ id: postId });
        const author = await post.author;
        if (user.username === author.username) {
            return true;
        } else {
            return false;
        }
    }

    async getAllPosts(Query?): Promise<PostShowWithoutCommentsDTO[]> {
        const allPost = await this.postRepository.find({ isDeleted: false });
        const postDTO = Promise.all(allPost.map(async (x: Post) => await this.createPostShowDTO(x)));
        const newkey = Object.keys(Query).toString();
        if (!!newkey) {
            return await plainToClass(PostShowWithCommentsDTO, (await postDTO).slice(0, +newkey));
        }
        return await plainToClass(PostShowWithCommentsDTO, await postDTO);
    }

    async createPostShowDTO(post: Post): Promise<PostShowWithCommentsDTO> {
        const comments = await post.comments;
        const postDTO = await {
            id: post.id,
            title: post.title,
            description: post.description,
            body: post.body,
            comments: await Promise.all((comments.filter((comment: Comments) => comment.isDeleted === false))
                .map(async (x: Comments) => await this.commentsService.createCommentDTO(x))),
            author: await post.author,
            datePosted: post.datePosted,
            dateModified: post.dateModified,
            like: await Promise.all((await post.like).map(async (x: User) => await plainToClass(UserShowDTO, x))),
            dislike: await Promise.all((await post.dislike).map(async (x: User) => await plainToClass(UserShowDTO, x))),
            flag: post.flag,
            isLocked: post.isLocked,
        };
        return await plainToClass(PostShowWithCommentsDTO, postDTO);
    }

    async getPostById(id: string): Promise<PostShowWithCommentsDTO> | undefined {
        const post = await this.postRepository.findOne({ id, isDeleted: false });
        if (!post) {
            return undefined;
        }
        const postDTO = await this.createPostShowDTO(post);
        return plainToClass(PostShowWithCommentsDTO, postDTO);
    }

    async create(createPost: CreatePostDTO, user: UserShowDTO): Promise<PostShowWithoutCommentsDTO> {
        const newPost = await this.postRepository.create(createPost);
        const author = await this.userRepository.findOne({ username: user.username });
        newPost.author = Promise.resolve(author);
        const createdPost = await this.postRepository.save(newPost);
        const postDTO = await this.createPostShowDTO(createdPost);
        return plainToClass(PostShowWithoutCommentsDTO, postDTO);
    }

    async updatePost(id: string, updatePost: UpdatePostDTO): Promise<PostShowWithoutCommentsDTO> {
        const postToBeUpdated: Post = await this.postRepository.findOne({ id, isDeleted: false, isLocked: false });
        if (!postToBeUpdated) {
            return undefined;
        }
        const updatedPost: Post = postToBeUpdated;
        updatedPost.body = updatePost.body;
        updatedPost.title = updatePost.title;
        updatedPost.description = updatePost.description;
        const savedPost = await this.postRepository.save(updatedPost);
        const postDTO = await this.createPostShowDTO(savedPost);
        return plainToClass(PostShowWithoutCommentsDTO, postDTO);
    }

    async deletePost(id: string, user: UserShowDTO): Promise<PostShowWithoutCommentsDTO> {
        const postToBeDeleted: Post = await this.postRepository.findOne({ id, isDeleted: false });
        if (!postToBeDeleted || (await postToBeDeleted.author).username !== user.username) {
            return undefined;
        }
        postToBeDeleted.isDeleted = true;
        const deletedPost = await this.postRepository.save(postToBeDeleted);
        const postDTO = await this.createPostShowDTO(deletedPost);
        return plainToClass(PostShowWithoutCommentsDTO, postDTO);
    }

    async filterByUser(param): Promise<PostShowWithoutCommentsDTO[]> {
        const user = await this.userRepository.findOne({ id: param, isDeleted: false });
        const foundPosts = await user.posts;
        const postDTO = Promise.all(foundPosts.map(async (x: Post) => await this.createPostShowDTO(x)));
        return await plainToClass(PostShowWithoutCommentsDTO, await postDTO);
    }

    async filterBySort(Query): Promise<PostShowWithoutCommentsDTO[]> {
        const data: any = await this.postRepository.find({ isDeleted: false });
        const newkey = Object.keys(Query).toString();

        if (newkey === 'author') {
            const result = await data.sort((a, b) =>
                a.author.username > b.author.username ? 1 : b.author.username > a.author.username ? -1 : 0,
            );
            const posts = await result;
            const postDTO = Promise.all(posts.map(async (x: Post) => await this.createPostShowDTO(x)));
            return plainToClass(PostShowWithoutCommentsDTO, await postDTO);
        }
        if (newkey === 'date') {
            const result = await data.sort((a, b) =>
                a.datePosted > b.datePosted
                    ? 1
                    : b.datePosted > a.datePosted
                        ? -1
                        : 0,
            );
            const posts = await result;
            const postDTO = Promise.all(posts.map(async (x: Post) => await this.createPostShowDTO(x)));
            return plainToClass(PostShowWithoutCommentsDTO, await postDTO);
        }
    }

    async likePost(user: UserShowDTO, id: string): Promise<PostShowWithoutCommentsDTO> {
        const postToLike: Post = await this.postRepository.findOne({ id, isDeleted: false, isLocked: false });
        if (!postToLike) {
            return undefined;
        }
        const userEntity: any = await this.userRepository.findOne({ username: user.username });
        const likes = await postToLike.like;
        const userFound: User = await likes.find((x: User) => x.username === userEntity.username);
        const dislike = await postToLike.dislike;
        const disUser: User = await dislike.find((x: User) => x.username === userEntity.username);
        if (!!userFound) {
            const index = likes.indexOf(userFound);
            likes.splice(+index, 1);
            postToLike.like = Promise.resolve(likes);
            const unlikedPost = await this.postRepository.save(postToLike);
            const postDTO = await this.createPostShowDTO(unlikedPost);
            return plainToClass(PostShowWithoutCommentsDTO, postDTO);
        } else if (!!disUser) {
            const index = dislike.indexOf(userFound);
            dislike.splice(+index, 1);
            postToLike.dislike = Promise.resolve(dislike);
            await this.postRepository.save(postToLike);
        }
        postToLike.like = Promise.resolve([userEntity, ...await postToLike.like]);
        const likedPost = await this.postRepository.save(postToLike);
        const postDTO = await this.createPostShowDTO(likedPost);
        return plainToClass(PostShowWithoutCommentsDTO, postDTO);
    }

    async dislikePost(user: UserShowDTO, id: string): Promise<PostShowWithoutCommentsDTO> {
        const postToDislike: any = await this.postRepository.findOne({ id, isDeleted: false, isLocked: false });
        if (!postToDislike) {
            return undefined;
        }
        const userEntity: User = await this.userRepository.findOne({ username: user.username });
        const likes = await postToDislike.like;
        const userFound: User = await likes.find((x: User) => x.username === userEntity.username);
        const dislike = await postToDislike.dislike;
        const disUser: User = await dislike.find((x: User) => x.username === userEntity.username);
        if (!!disUser) {
            const index = dislike.indexOf(userFound);
            dislike.splice(+index, 1);
            postToDislike.dislike = Promise.resolve(dislike);
            this.postRepository.save(postToDislike);
            const undislikedPost = await this.postRepository.save(postToDislike);
            const postDTO = await this.createPostShowDTO(undislikedPost);
            return plainToClass(PostShowWithoutCommentsDTO, postDTO);
        } else if (!!userFound) {
            const index = likes.indexOf(userFound);
            likes.splice(+index, 1);
            postToDislike.like = Promise.resolve(likes);
            await this.postRepository.save(postToDislike);
        }
        postToDislike.dislike = Promise.resolve([userEntity, ...await postToDislike.dislike]);
        const dislikedPost = await this.postRepository.save(postToDislike);
        const postDTO = await this.createPostShowDTO(dislikedPost);
        return plainToClass(PostShowWithoutCommentsDTO, postDTO);
    }

    async lockPost(id: string): Promise<PostShowWithoutCommentsDTO> {
        const postToBeLocked: Post = await this.postRepository.findOne({ id, isDeleted: false, isLocked: false });
        if (!postToBeLocked) {
            return undefined;
        }
        postToBeLocked.isLocked = true;
        const lockedPost = await this.postRepository.save(postToBeLocked);
        const postDTO = await this.createPostShowDTO(lockedPost);
        return plainToClass(PostShowWithoutCommentsDTO, postDTO);
    }

    async flagPost(id, user, reason: FlagPostDTO): Promise<any> {
        const postToFlag: Post = await this.postRepository.findOne({ id, isDeleted: false });
        if (!postToFlag) {
            return undefined;
        }
        const newFlag: FlagPostEntity = await this.flagPostRepository.create(reason);
        const userEntity: User = await this.userRepository.findOne({ username: user.username, isDeleted: false });
        newFlag.userId = Promise.resolve(userEntity);
        newFlag.postId = postToFlag;
        return await this.flagPostRepository.save(newFlag);
    }
}
