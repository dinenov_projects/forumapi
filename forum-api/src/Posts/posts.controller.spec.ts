import { Test, TestingModule } from '@nestjs/testing';
import { PostsController } from './posts.controller';
import { UsersService } from '../core/services/users.service';
import { PostsService } from './posts.service';
import { NotFoundException, UnauthorizedException } from '@nestjs/common';
import { CreatePostDTO } from '../models/posts/create-post.dto';

describe('Posts Controller', () => {
  let controller: PostsController;

  const testUser = {
    id: '1',
    username: 'username',
    banstatus: {
      isBanned: false,
      description: 'description',
    },
    roles: [{
      name: 'User',
    }],
  };

  const mockUsersService = {
    getUserById: (userId) => {
      if (userId === testUser.id) {
        return testUser;
      } else {
        return undefined;
      }
    },
  };

  const mockPostsService = {
    getAllPosts: (query: string) => ['Posts'],
    filterByUser: (userId: string) => ['Posts'],
    filterBySort: (query: string) => ['Posts'],
    create: (post: any, user: any) => 'Post',
    updatePost: (postId: string, post: any) => {
      if (postId === 'postId') {
        return 'Post';
      }
      return undefined;
    },
    deletePost: (postId: string, user: any) => {
      if (postId === 'postId') {
        return 'Post';
      }
      return undefined;
    },
    lockPost: (postId: string) => {
      if (postId === 'postId') {
        return 'Post';
      }
      return undefined;
    },
    flagPost: (postId: string, user: any, reason: any) => {
      if (postId === 'postId') {
        return 'Post';
      }
      return undefined;
    },
    likePost: (user: any, postId: string) => {
      if (postId === 'postId') {
        return 'Post';
      }
      return undefined;
    },
    dislikePost: (user: any, postId: string) => {
      if (postId === 'postId') {
        return 'Post';
      }
      return undefined;
    },
    isUserCreator: (user: any, postId: string) => {
      if (user.id === '1') {
        return true;
      }
      return false;
    },
    getPostById: (postId: string) => {
      if (postId === '1') {
        return 'Post';
      }
      return undefined;
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostsController],
      providers: [
        {
          provide: PostsService,
          useValue: mockPostsService,
        },
        {
          provide: UsersService,
          useValue: mockUsersService,
        },
      ],
    }).compile();

    controller = module.get<PostsController>(PostsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getPosts', () => {
    it('getPosts should return correct values', async () => {
      const result = await controller.getPosts('1');
      expect(result).toEqual(['Posts']);
    });

    it('getPosts should call getAllPosts', async () => {
      const findUseById = jest.spyOn(mockPostsService, 'getAllPosts');
      await controller.getPosts('1');
      expect(findUseById).toBeCalledTimes(1);
      findUseById.mockRestore();
    });
  });

  describe('filterByUser', () => {
    it('filterByUser should return correct values', async () => {
      const result = await controller.filterByUser('1');
      expect(result).toEqual(['Posts']);
    });

    it('filterByUser should call usersService.getUserById', async () => {
      const getUserById = jest.spyOn(mockUsersService, 'getUserById');
      await controller.filterByUser('1');
      expect(getUserById).toBeCalledTimes(1);
      getUserById.mockRestore();
    });

    it('filterByUser should call postsService.filterByUser', async () => {
      const filterByUser = jest.spyOn(mockPostsService, 'filterByUser');
      await controller.filterByUser('1');
      expect(filterByUser).toBeCalledTimes(1);
      filterByUser.mockRestore();
    });

    it('filterByUser should throw, when searching for posts of nonexisting user', async () => {
      try {
        controller.filterByUser('0');
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }
    });

    it('filterByUser should not call postsService.filterBuUser, when searching for posts of nonexisting user', async () => {
      const filterByUser = jest.spyOn(mockPostsService, 'filterByUser');
      try {
        controller.filterByUser('0');
      } catch (err) {
        expect(filterByUser).toBeCalledTimes(0);
      }
      filterByUser.mockRestore();
    });
  });

  describe('filterBySort', () => {
    it('filterBySort should return correct values', async () => {
      const result = await controller.filterBySort('query');
      expect(result).toEqual(['Posts']);
    });

    it('filterBySort should call postsService.filterBySort', async () => {
      const filterBySort = jest.spyOn(mockPostsService, 'filterBySort');
      await controller.filterBySort('query');
      expect(filterBySort).toBeCalledTimes(1);
      filterBySort.mockRestore();
    });
  });

  describe('create', () => {
    it('create should return correct values', async () => {
      const post: CreatePostDTO = {
        title: 'title',
        description: 'description',
        body: 'body',
      };

      const result = await controller.create(post, testUser);
      expect(result).toEqual({
        message: 'Post has been submitted successfully!',
        data: 'Post',
        });
    });

    it('create should call postsService.create', async () => {
      const post: CreatePostDTO = {
        title: 'title',
        description: 'description',
        body: 'body',
      };
      const create = jest.spyOn(mockPostsService, 'create');
      await controller.create(post, testUser);
      expect(create).toBeCalledTimes(1);
      create.mockRestore();
    });
  });

  describe('getPost', () => {
    it('getPost should return correct values', async () => {
      const result = await controller.getPost('1');
      expect(result).toBe('Post');
    });

    it('getPost should call postsService.getPostById', async () => {
      const getPostById = jest.spyOn(mockPostsService, 'getPostById');
      await controller.getPost('1');
      expect(getPostById).toBeCalledTimes(1);
      getPostById.mockRestore();
    });

    it('getPost should throw, when searching for nonexisting post', async () => {
      try {
        await controller.getPost('0');
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }
    });
  });

  describe('updatePost', () => {
    it('updatePost should return correct values', async () => {
      const post: any = {};
      const result = await controller.updatePost('postId', post, testUser, true);
      expect(result).toEqual({
        message: 'Post has been updated successfully!',
        data: 'Post',
      });
    });

    it('updatePost should throw when the user is not author or admin', async () => {
      const post: any = {};
      const unauthorizedUser: any = {id: '0'};
      try {
        await controller.updatePost('postId', post, unauthorizedUser, false);
      } catch (err) {
        expect(err).toBeInstanceOf(UnauthorizedException);
      }
    });

    it('updatePost should throw when trying to update nonexisting post', async () => {
      const post: any = {};
      try {
        await controller.updatePost('emptyId', post, testUser, false);
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }
    });

    it('updatePost should call postService.isUserCreator', async () => {
      const post: any = {};
      const isUserCreator = jest.spyOn(mockPostsService, 'isUserCreator');
      await controller.updatePost('postId', post, testUser, true);
      expect(isUserCreator).toBeCalledTimes(1);
      isUserCreator.mockRestore();
    });

    it('updatePost should call postService.updatePost', async () => {
      const post: any = {};
      const updatePost = jest.spyOn(mockPostsService, 'updatePost');
      await controller.updatePost('postId', post, testUser, true);
      expect(updatePost).toBeCalledTimes(1);
      updatePost.mockRestore();
    });
  });

  describe('deletePost', () => {
    it('deletePost should return correct values', async () => {
      const result = await controller.deletePost('postId', testUser, true);
      expect(result).toEqual({
        message: 'Post has been deleted successfully!',
        data: 'Post',
      });
    });

    it('deletePost should throw when the user is not author or admin', async () => {
      const unauthorizedUser: any = {id: '0'};
      try {
        await controller.deletePost('postId', unauthorizedUser, false);
      } catch (err) {
        expect(err).toBeInstanceOf(UnauthorizedException);
      }
    });

    it('deletePost should throw when trying to update nonexisting post', async () => {
      try {
        await controller.deletePost('emptyId', testUser, false);
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }
    });

    it('deletePost should call postService.isUserCreator', async () => {
      const isUserCreator = jest.spyOn(mockPostsService, 'isUserCreator');
      await controller.deletePost('postId', testUser, true);
      expect(isUserCreator).toBeCalledTimes(1);
      isUserCreator.mockRestore();
    });

    it('deletePost should call postService.deletePost', async () => {
      const deletePost = jest.spyOn(mockPostsService, 'deletePost');
      await controller.deletePost('postId', testUser, true);
      expect(deletePost).toBeCalledTimes(1);
      deletePost.mockRestore();
    });
  });

  describe('lockPost', () => {
    it('lockPost should return correct values', async () => {
      const result = await controller.lockPost('postId', testUser, true);
      expect(result).toEqual({
        message: 'Post has been locked successfully!',
        data: 'Post',
      });
    });

    it('lockPost should throw when the user is not author or admin', async () => {
      const unauthorizedUser: any = {id: '0'};
      try {
        await controller.lockPost('postId', unauthorizedUser, false);
      } catch (err) {
        expect(err).toBeInstanceOf(UnauthorizedException);
      }
    });

    it('lockPost should throw when trying to update nonexisting post', async () => {
      try {
        await controller.lockPost('emptyId', testUser, false);
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }
    });

    it('lockPost should call postService.isUserCreator', async () => {
      const isUserCreator = jest.spyOn(mockPostsService, 'isUserCreator');
      await controller.lockPost('postId', testUser, true);
      expect(isUserCreator).toBeCalledTimes(1);
      isUserCreator.mockRestore();
    });

    it('lockPost should call postService.lockPost', async () => {
      const lockPost = jest.spyOn(mockPostsService, 'lockPost');
      await controller.lockPost('postId', testUser, true);
      expect(lockPost).toBeCalledTimes(1);
      lockPost.mockRestore();
    });
  });

  describe('flagPost', () => {
    it('flagPost should return correct values', async () => {
      const flagPostDto: any = {};
      const result = await controller.flagPost('postId', testUser, flagPostDto);
      expect(result).toEqual({
        message: 'Post has been Flagged successfully!',
        data: 'Post',
      });
    });

    it('flagPost should throw when trying to update nonexisting post', async () => {
      const flagPostDto: any = {};
      try {
        await controller.flagPost('emptyId', testUser, flagPostDto);
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }
    });

    it('flagPost should call postService.flagPost', async () => {
      const flagPostDto: any = {};
      const flagPost = jest.spyOn(mockPostsService, 'flagPost');
      await controller.flagPost('postId', testUser, flagPostDto);
      expect(flagPost).toBeCalledTimes(1);
      flagPost.mockRestore();
    });
  });

  describe('likePost', () => {
    it('likePost should return correct values', async () => {
      const result = await controller.likePost(testUser, 'postId');
      expect(result).toEqual({
        message: 'Post has been liked successfully!',
        data: 'Post',
      });
    });

    it('likePost should throw when trying to update nonexisting post', async () => {
      try {
        await controller.likePost(testUser, 'emptyId');
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }
    });

    it('likePost should call postService.likePost', async () => {
      const likePost = jest.spyOn(mockPostsService, 'likePost');
      await controller.likePost(testUser, 'postId');
      expect(likePost).toBeCalledTimes(1);
      likePost.mockRestore();
    });
  });

  describe('likePost', () => {
    it('dislikePost should return correct values', async () => {
      const result = await controller.dislikePost(testUser, 'postId');
      expect(result).toEqual({
        message: 'Post has been disliked successfully!',
        data: 'Post',
      });
    });

    it('dislikePost should throw when trying to update nonexisting post', async () => {
      try {
        await controller.dislikePost(testUser, 'emptyId');
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }
    });

    it('dislikePost should call postService.dislikePost', async () => {
      const dislikePost = jest.spyOn(mockPostsService, 'dislikePost');
      await controller.dislikePost(testUser, 'postId');
      expect(dislikePost).toBeCalledTimes(1);
      dislikePost.mockRestore();
    });
  });
});
