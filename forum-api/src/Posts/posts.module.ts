import { Module } from '@nestjs/common';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Post } from '../data/entities/post.entity';
import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { User } from '../data/entities/user.entity';
import { FlagPostEntity } from '../data/entities/flag.entity';
import { CommentsModule } from '../comments/comments.module';
@Module({
  imports: [
    CoreModule, AuthModule, CommentsModule, TypeOrmModule.forFeature([Post, User, FlagPostEntity]),
  ],
  controllers: [PostsController],
  providers: [PostsService],
  exports: [PostsService],
})
export class PostsModule { }
