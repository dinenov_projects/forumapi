import {
    Controller,
    Post,
    Put,
    Delete,
    UseGuards,
    Param,
    Body,
    UsePipes,
    ValidationPipe,
    Get,
    NotFoundException,
    UnauthorizedException,
    HttpStatus,
    HttpCode,
} from '@nestjs/common';
import { CommentsService } from './comments.service';
import { AuthGuard } from '@nestjs/passport';
import { CreateCommentDTO } from '../models/comments/create-comment.dto';
import { UpdateCommentDTO } from '../models/comments/update-comment.dto';
import { UserShowDTO } from '../models/user';
import { User } from '../common/decorators/user.decorator';
import { ShowCommentDTO } from '../models/comments/show-comment.dto';
import { JwtAuthGuard } from '../common/guards/jwt-auth.guard';
import { IsAdmin } from '../common/decorators/is-admin.decorator';
import { BanstatusGuard } from '../common/guards/banstatus.guard';

@Controller('posts')
export class CommentsController {
    constructor(
        private readonly commentsService: CommentsService,
    ) { }

    @Post(':postId/comments')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, BanstatusGuard)
    @UsePipes(
        new ValidationPipe({
            transform: true,
            whitelist: true,
        }),
    )
    async addComment(
        @User() user: UserShowDTO,
        @Param('postId') postId: string,
        @Body() comment: CreateCommentDTO,
    ): Promise<any> {
        const createdComment = await this.commentsService.addComment(comment, postId, user);
        if (!createdComment) {
            throw new NotFoundException('This post was not found or is locked');
        }
        return {
            message: 'Comment has been submitted successfully!',
            data: createdComment,
        };
    }

    @Put(':postId/comments/:commentId')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, BanstatusGuard)
    @UsePipes(
        new ValidationPipe({
            transform: true,
            whitelist: true,
        }),
    )
    async updateComment(
        @User() user: UserShowDTO,
        @Param('postId') postId: string,
        @Param('commentId') commentId: string,
        @Body() comment: UpdateCommentDTO,
        @IsAdmin() isAdmin: boolean,
    ): Promise<any> {
        const commentToBeUpdated = this.showComment(commentId);
        if (!commentToBeUpdated) {
            throw new NotFoundException('Comment not found!');
        } else if (((await commentToBeUpdated).author.username !== user.username) && !isAdmin) {
            throw new UnauthorizedException('Unauthorized operation!');
        }
        const createdComment = await this.commentsService.updateComment(comment, commentId, user);
        return {
            message: 'Comment has been updated successfully!',
            data: createdComment,
        };
    }

    @Delete(':postId/comments/:commentId')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, BanstatusGuard)
    async deleteComment(
        @User() user: UserShowDTO,
        @Param('postId') postId: string,
        @Param('commentId') commentId: string,
        @IsAdmin() isAdmin: boolean,
    ): Promise<any> {
        const commentToBeDeleted = this.showComment(commentId);
        if (!commentToBeDeleted) {
            throw new NotFoundException('Comment not found!');
        } else if (((await commentToBeDeleted).author.username !== user.username) && !isAdmin) {
            throw new UnauthorizedException('Unauthorized operation!');
        }
        const createdComment = await this.commentsService.deleteComment(commentId, user);
        return {
            message: 'Comment has been deleted successfully!',
            data: createdComment,
        };
    }

    @Get(':postId/comments/:commentId')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard)
    async showComment(
        @Param('commentId') commentId: string,
    ): Promise<ShowCommentDTO> {
        const comment = await this.commentsService.showComment(commentId);
        if (!comment) {
            throw new NotFoundException('Comment not found!');
        }
        return await comment;
    }

}

