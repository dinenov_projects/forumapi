import { Test, TestingModule } from '@nestjs/testing';
import { CommentsService } from './comments.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Comments } from '../data/entities/comment.entity';
import { Post } from '../data/entities/post.entity';
import { User } from '../data/entities/user.entity';

describe('CommentsService', () => {
  let service: CommentsService;
  const commentsRepository = {};
  const postRepository = {};
  const userRepository = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CommentsService,
        {
          provide: getRepositoryToken(Comments),
          useValue: commentsRepository,
        },
        { provide: getRepositoryToken(Post), useValue: postRepository },
        { provide: getRepositoryToken(User), useValue: userRepository },
      ],
    }).compile();
    service = module.get<CommentsService>(CommentsService);

  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
