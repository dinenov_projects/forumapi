import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserShowDTO } from 'src/models/user';
import { CreateCommentDTO } from '../models/comments/create-comment.dto';
import { Post } from '../data/entities/post.entity';
import { User } from '../data/entities/user.entity';
import { Comments } from '../data/entities/comment.entity';
import { plainToClass } from 'class-transformer';
import { ShowCommentDTO } from '../models/comments/show-comment.dto';

@Injectable()
export class CommentsService {
    constructor(
        @InjectRepository(Comments) private readonly commentsRepository: Repository<Comments>,
        @InjectRepository(Post) private readonly postsRepository: Repository<Post>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
    ) { }

    async createCommentDTO(comment: Comments): Promise<ShowCommentDTO> {
        const commentDTO = {
            id: comment.id,
            body: comment.body,
            author: await comment.author,
            dateCreated: comment.dateCreated,
            dateModified: comment.dateModified,
        };
        return await plainToClass(ShowCommentDTO, commentDTO);
    }

    async addComment(comment: CreateCommentDTO, postId: string, user: UserShowDTO): Promise<ShowCommentDTO> {
        const commentToBeCreated = await this.commentsRepository.create(comment);
        const postOfTheComment = await this.postsRepository.findOne({ id: postId, isDeleted: false, isLocked: false });
        if (!postOfTheComment) {
            return undefined;
        }
        const author = await this.usersRepository.findOne({ username: user.username });
        commentToBeCreated.post = Promise.resolve(postOfTheComment);
        commentToBeCreated.author = Promise.resolve(author);
        const createdComment = await this.commentsRepository.save(commentToBeCreated);
        return this.createCommentDTO(createdComment);
    }

    async updateComment(comment: CreateCommentDTO, commentId: string, user: any): Promise<ShowCommentDTO> {
        const commentToBeUpdated = await this.commentsRepository.findOne({ id: commentId, isDeleted: false });
        if (!commentToBeUpdated) {
            return undefined;
        }
        commentToBeUpdated.body = comment.body;
        const updatedComment = await this.commentsRepository.save(commentToBeUpdated);
        return this.createCommentDTO(updatedComment);
    }

    async deleteComment(commentId: string, user: any): Promise<ShowCommentDTO> {
        const commentToBeDeleted = await this.commentsRepository.findOne({ id: commentId, isDeleted: false });
        if (!commentToBeDeleted) {
            return undefined;
        }
        commentToBeDeleted.isDeleted = true;
        const deletedComment = await this.commentsRepository.save(commentToBeDeleted);
        return this.createCommentDTO(deletedComment);
    }

    async showComment(commentId: string): Promise<ShowCommentDTO | undefined> {
        const comment = await this.commentsRepository.findOne({ id: commentId, isDeleted: false });
        if (!!comment) {
            return this.createCommentDTO(comment);
        } else {
            return undefined;
        }
    }
}

