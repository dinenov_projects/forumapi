import { Test, TestingModule } from '@nestjs/testing';
import { CommentsController } from './comments.controller';
import { CreateCommentDTO } from '../models/comments/create-comment.dto';
import { UserShowDTO } from '../models/user/user-show.dto';
import { CommentsService } from './comments.service';

describe('Comments Controller', () => {
  let controller: CommentsController;
  const commentsService = {
    addComment() { }
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CommentsController],
      providers: [
        {
          provide: CommentsService,
          useValue: commentsService

        }
      ]
    }).compile();

    controller = module.get<CommentsController>(CommentsController);
  });



  describe('addComment', () => {
    it('should call addComment', async () => {
      const testComment: CreateCommentDTO = {
        body: 'test string',
      };
      const testPostId: string = '1';
      const testUser: UserShowDTO = {
        id: '1',
        username: 'testUser',
        banstatus: {
          isBanned: false,
          description: 'test',
        },
        roles: [
          {
            name: 'testname',
          },
        ],
      };
      const fakeToken = 'token';

      const addCommentSpy = jest
        .spyOn(commentsService, 'addComment')
        .mockImplementation(() => Promise.resolve(fakeToken));

      await controller.addComment(testUser, testPostId, testComment)

      expect(addCommentSpy).toBeCalled()

      addCommentSpy.mockRestore()


    })
  })
});
