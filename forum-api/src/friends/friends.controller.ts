import { Controller, Post, UseGuards, Param, Delete, Get, UnauthorizedException } from '@nestjs/common';
import { UserShowDTO } from '../models/user';
import { FriendsService } from './friends.service';
import { User } from '../common/decorators/user.decorator';
import { JwtAuthGuard } from '../common/guards/jwt-auth.guard';
import { AuthGuard } from '@nestjs/passport';
import { ActivityShowDTO } from '../models/activity/activity-show.dto';
import { IsAdmin } from '../common/decorators/is-admin.decorator';
import { BanstatusGuard } from '../common/guards/banstatus.guard';

@Controller()
export class FriendsController {
    constructor(
        private readonly friendsService: FriendsService,
    ) { }

    @Get('friends')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard)
    async getFriends(
        @User() user: UserShowDTO,
    ): Promise<any> {
        return await this.friendsService.getFriends(user);
    }

    @Get('users/:userId/friends')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard)
    async areFriends(
        @Param('userId') userId: string,
        @User() user: UserShowDTO,
    ): Promise<any> {
        return await this.friendsService.areFriendsResponse(userId, user);
    }

    @Get('users/:userId/friends/requesting')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard)
    async hasFriendsRequest(
        @Param('userId') userId: string,
        @User() user: UserShowDTO,
    ): Promise<any> {
        return await this.friendsService.hasFriendsRequest(userId, user);
    }

    @Get('users/:userId/friends/requested')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard)
    async hasSentFriendsRequest(
        @Param('userId') userId: string,
        @User() user: UserShowDTO,
    ): Promise<any> {
        return await this.friendsService.hasSentFriendsRequest(userId, user);
    }

    @Get('users/:userId/activity')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, BanstatusGuard)
    async getActivity(
        @Param('userId') userId: string,
        @User() user: UserShowDTO,
        @IsAdmin() isAdmin: boolean,
    ): Promise<ActivityShowDTO> {
        if (!(await this.friendsService.areFriends(userId, user)) && !isAdmin && userId.toString() !== user.id.toString()) {
            throw new UnauthorizedException('Unauthorized access!');
        }
        return await this.friendsService.getUserActivity(userId);
    }

    @Post('users/:userId/follow')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, BanstatusGuard)
    async followUser(
        @User() user: UserShowDTO,
        @Param('userId') userId: string,
    ): Promise<any> {
        return {
            message: 'User has been successfully followed!',
            data: await this.friendsService.followUser(user, userId),
        };
    }

    @Delete('users/:userId/follow')
    @UseGuards(AuthGuard('jwt'), JwtAuthGuard, BanstatusGuard)
    async unfollowUser(
        @User() user: UserShowDTO,
        @Param('userId') userId: string,
    ): Promise<any> {
        return {
            message: 'User has been successfully unfollowed!',
            data: await this.friendsService.unfollowUser(user, userId),
        };
    }
}
