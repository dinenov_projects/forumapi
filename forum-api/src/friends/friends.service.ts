import { Injectable } from '@nestjs/common';
import { UserShowDTO } from '../models/user';
import { UsersService } from '../core/services/users.service';
import { User } from '../data/entities/user.entity';
import { ActivityShowDTO } from '../models/activity/activity-show.dto';
import { plainToClass } from 'class-transformer';

@Injectable()
export class FriendsService {
    constructor(
        private readonly usersService: UsersService,
    ) { }

    async followUser(user: UserShowDTO, followingId: string): Promise<UserShowDTO> | undefined {
        return this.usersService.followUser(user, followingId);
    }

    async unfollowUser(user: UserShowDTO, followingId: string): Promise<UserShowDTO> | undefined {
        return this.usersService.unfollowUser(user, followingId);
    }

    async getFriends(user: UserShowDTO): Promise<any> {
        return this.usersService.getFriends(user);
    }

    async getUserById(userId: string): Promise<User> {
        return await this.usersService.getUserById(userId);
    }

    async getUserActivity(userId: string): Promise<ActivityShowDTO> {
        const user = await this.getUserById(userId);
        const activity = {
            id: user.id,
            username: user.username,
            posts: (await user.posts).reverse(),
            comments: (await user.comments).reverse(),
            likes: (await user.likes).reverse(),
            dislikes: (await user.dislikes).reverse(),
        };
        return plainToClass(ActivityShowDTO, activity);
    }

    async areFriends(userId: string, user: UserShowDTO): Promise<boolean> {
        const friends: any = await this.getFriends(user);
        let userIsFollowedByUser: boolean = false;
        let userIsFollowingUser: boolean = false;
        friends.followers.forEach((x: User) => {
            if (x.id.toString() === userId) {
                userIsFollowedByUser = true;
            }
        });
        friends.following.forEach((x: User) => {
            if (x.id.toString() === userId) {
                userIsFollowingUser = true;
            }
        });
        if (userIsFollowedByUser && userIsFollowingUser) {
            return true;
        }
        return false;
    }

    async areFriendsResponse(userId: string, user: UserShowDTO): Promise<boolean> {
        const friends: any = await this.getFriends(user);
        let userIsFollowedByUser: boolean = false;
        let userIsFollowingUser: boolean = false;
        friends.followers.forEach((x: User) => {
            if (x.id.toString() === userId) {
                userIsFollowedByUser = true;
            }
        });
        friends.following.forEach((x: User) => {
            if (x.id.toString() === userId) {
                userIsFollowingUser = true;
            }
        });
        if (userIsFollowedByUser && userIsFollowingUser) {
            return true;
        }
        return false;
    }

    async hasFriendsRequest(userId: string, user: UserShowDTO): Promise<boolean> {
        const friends: any = await this.getFriends(user);
        let userIsFollowedByUser: boolean = false;
        let userIsFollowingUser: boolean = false;
        friends.followers.forEach((x: User) => {
            if (x.id.toString() === userId) {
                userIsFollowedByUser = true;
            }
        });
        friends.following.forEach((x: User) => {
            if (x.id.toString() === userId) {
                userIsFollowingUser = true;
            }
        });
        if (userIsFollowedByUser && !userIsFollowingUser) {
            return true;
        }
        return false;
    }

    async hasSentFriendsRequest(userId: string, user: UserShowDTO): Promise<boolean> {
        const friends: any = await this.getFriends(user);
        let userIsFollowedByUser: boolean = false;
        let userIsFollowingUser: boolean = false;
        friends.followers.forEach((x: User) => {
            if (x.id.toString() === userId) {
                userIsFollowedByUser = true;
            }
        });
        friends.following.forEach((x: User) => {
            if (x.id.toString() === userId) {
                userIsFollowingUser = true;
            }
        });
        if (!userIsFollowedByUser && userIsFollowingUser) {
            return true;
        }
        return false;
    }
}
