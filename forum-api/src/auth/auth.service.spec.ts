import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../core/services/users.service';
import { BlacklistService } from './blacklist/blacklist.service';
import { UserRegisterDTO } from '../models/user/user-register.dto';
import { UserShowDTO } from '../models/user/user-show.dto';
import { UserLoginDTO } from '../models/user/user-login.dto';
import { JwtPayload } from '../core/interfaces/jwt.payload';
describe('AuthService', () => {
  let service: AuthService;
  const mockJwtService = {
    signAsync() { },
  };
  const mockUserService = {
    register() { },
    findUsername() { },
    findEmail() { },
    validateUserPassword() { },
    validateUserFromPayload() { },
    checkForRolesAndCreateThem() { }
  };
  const mockBlacklistService = {
    blacklist() { },
    checkIfBlacklisted() { }
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: JwtService,
          useValue: mockJwtService,
        },
        {
          provide: UsersService,
          useValue: mockUserService,
        },
        {
          provide: BlacklistService,
          useValue: mockBlacklistService,
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('register', () => {
    const testUser: UserRegisterDTO = {
      username: 'TestUser',
      password: 'TestPass',
      email: 'test@test.com',
    };

    const responceToREgister = {
      id: 1,
      username: 'TestUser',
      banstatus: {
        isBanned: false,
        description: 'This user is not banned!',
      },
      roles: [
        {
          name: 'User',
        },
      ],
    };
    it('registerUser should return correct values', async () => {
      const registerSpy = jest
        .spyOn(mockUserService, 'register')
        .mockImplementation(() => Promise.resolve(responceToREgister));
      const result = await service.register(testUser);
      expect(result).toEqual(responceToREgister)
      expect(registerSpy).toBeCalledTimes(1);
      expect(registerSpy).toHaveBeenCalledWith(testUser);
      registerSpy.mockRestore();
    });
    it('register should call usersService.register', async () => {
      const registerSpy = jest
        .spyOn(mockUserService, 'register')
        .mockImplementation(() => Promise.resolve());
      await service.register(testUser);
      expect(registerSpy).toBeCalledTimes(1);
      expect(registerSpy).toHaveBeenCalledWith(testUser);
      registerSpy.mockRestore();
    });
  });
  describe('login', () => {
    const testUser: UserShowDTO = {
      id: '1',
      username: 'test',
      banstatus: {
        isBanned: false,
        description: 'This user is not banned!',
      },
      roles: [{ name: 'User' }],
    };
    const testUserLogin: UserLoginDTO = {
      username: 'test',
      password: 'testpass',
    };
    it('login should call findUserByUsername with correct values', async () => {
      const loginSpy = jest
        .spyOn(service, 'findUserByUsername')
        .mockImplementation(() => Promise.resolve(testUser));
      await service.login(testUserLogin);
      expect(loginSpy).toHaveBeenCalledTimes(1);
      expect(loginSpy).toHaveBeenCalledWith(testUserLogin.username);
      loginSpy.mockRestore();
    });
    it('login should return correct values', async () => {
      const mockToken = 'token';
      const jwtLogSpy = jest
        .spyOn(mockJwtService, 'signAsync')
        .mockImplementation(() => Promise.resolve(mockToken));

      const loginSpy = jest
        .spyOn(service, 'findUserByUsername')
        .mockImplementation(() => Promise.resolve(testUser));

      const result = await service.login(testUserLogin);
      expect(result).toEqual(mockToken)
      expect(jwtLogSpy).toHaveBeenCalled();
      expect(jwtLogSpy).toHaveBeenCalledWith(testUserLogin);
      jwtLogSpy.mockRestore();
      loginSpy.mockRestore();
    });
    it('login should return null if user is not found', async () => {
      const loginSpy = jest
        .spyOn(service, 'findUserByUsername')
        .mockImplementation(() => Promise.resolve(null));

      const result = await service.login(testUserLogin);

      expect(result).toEqual(null);

      loginSpy.mockRestore();
    });
    it('login should return user correct token', async () => {
      const mockToken = 'token';
      const jwtLogSpy = jest
        .spyOn(mockJwtService, 'signAsync')
        .mockImplementation(() => Promise.resolve(mockToken));

      const loginSpy = jest
        .spyOn(service, 'findUserByUsername')
        .mockImplementation(() => Promise.resolve(testUser));

      const result = await service.login(testUserLogin);
      expect(result).toEqual(mockToken)
      expect(jwtLogSpy).toHaveBeenCalled();
      expect(jwtLogSpy).toHaveBeenCalledWith(testUserLogin);
      jwtLogSpy.mockRestore();
      loginSpy.mockRestore();
    });
  });
  describe('logout', () => {
    it('logout shoud return correct value', async () => {
      const mockToken = 'token';
      const logoutSpy = jest
        .spyOn(mockBlacklistService, 'blacklist')
        .mockImplementation(() => Promise.resolve(mockToken));
      const result = await service.logout(mockToken)
      expect(result).toEqual(mockToken)
      logoutSpy.mockRestore()
    })
    it('logout shoud call blacklistService.blacklist with correnct value', async () => {
      const mockToken = 'token';
      const logoutSpy = jest
        .spyOn(mockBlacklistService, 'blacklist')
        .mockImplementation(() => Promise.resolve(mockToken));
      await service.logout(mockToken)
      expect(logoutSpy).toBeCalled()
      expect(logoutSpy).toHaveBeenCalledWith(mockToken)
      logoutSpy.mockRestore()
    })
  })
  describe('findUserByUsername', () => {
    it('findUserByUsername shoud call usersService.findUsername with correct value', async () => {
      const mockUsername = 'testUser'
      const findUserByUsernameSpy = jest
        .spyOn(mockUserService, 'findUsername')
        .mockImplementation(() => Promise.resolve(mockUsername))
      await service.findUserByUsername(mockUsername)
      expect(findUserByUsernameSpy).toBeCalledTimes(1)
      expect(findUserByUsernameSpy).toBeCalledWith(mockUsername)
      findUserByUsernameSpy.mockRestore()
    })
    it('findUserByUsername should return Undefined if wrong value is used', async () => {
      const mockUsername = 'testUser'
      const findUserByUsernameSpy = jest
        .spyOn(mockUserService, 'findUsername')
        .mockImplementation(() => Promise.resolve())
      const result = await service.findUserByUsername('tset')
      expect(findUserByUsernameSpy).toBeCalledTimes(1)
      expect(result).toBeUndefined()
      findUserByUsernameSpy.mockRestore()
    })
    it('findUserByUsername should return correct value', async () => {
      const mockUsername = 'testUser'
      const findUserByUsernameSpy = jest
        .spyOn(mockUserService, 'findUsername')
        .mockImplementation(() => Promise.resolve(mockUsername))
      const result = await service.findUserByUsername(mockUsername)
      expect(findUserByUsernameSpy).toBeCalledTimes(1)
      expect(result).toEqual(mockUsername)
      findUserByUsernameSpy.mockRestore()
    })
  })
  describe('findEmailInTheDatabase', () => {
    it('findEmailInTheDatabase shoud call usersService.findEmail with correct value', async () => {
      const mockEmail = 'test@test.com'
      const findEmailInTheDatabaseSpy = jest
        .spyOn(mockUserService, 'findEmail')
        .mockImplementation(() => Promise.resolve(mockEmail))
      await service.findEmailInTheDatabase(mockEmail)
      expect(findEmailInTheDatabaseSpy).toBeCalledTimes(1)
      expect(findEmailInTheDatabaseSpy).toBeCalledWith(mockEmail)
      findEmailInTheDatabaseSpy.mockRestore()
    })
    it('findEmailInTheDatabase should return Undefined if wrong value is used', async () => {
      const mockEmail = 'test@test.com'
      const findEmailInTheDatabaseSpy = jest
        .spyOn(mockUserService, 'findEmail')
        .mockImplementation(() => Promise.resolve())
      const result = await service.findEmailInTheDatabase("test")
      expect(findEmailInTheDatabaseSpy).toBeCalledTimes(1)
      expect(result).toBeUndefined()
      findEmailInTheDatabaseSpy.mockRestore()
    })
    it('findEmailInTheDatabase should return correct value', async () => {
      const mockEmail = 'test@test.com'
      const findEmailInTheDatabaseSpy = jest
        .spyOn(mockUserService, 'findEmail')
        .mockImplementation(() => Promise.resolve(mockEmail))
      const result = await service.findEmailInTheDatabase(mockEmail)
      expect(findEmailInTheDatabaseSpy).toBeCalledTimes(1)
      expect(result).toEqual(mockEmail)
      findEmailInTheDatabaseSpy.mockRestore()
    })
  })
  describe('validateUserPassword', () => {
    it('validateUserPassword should call usersService.validateUserPassword with correct value', async () => {
      const testUserLogin: UserLoginDTO = {
        username: 'test',
        password: 'testpass',
      };
      const findEmailInTheDatabaseSpy = jest
        .spyOn(mockUserService, 'validateUserPassword')
        .mockImplementation(() => Promise.resolve(testUserLogin))
      await service.validateUserPassword(testUserLogin)
      expect(findEmailInTheDatabaseSpy).toBeCalledTimes(1)
      expect(findEmailInTheDatabaseSpy).toBeCalledWith(testUserLogin)
      findEmailInTheDatabaseSpy.mockRestore()
    })
    it('validateUserPassword should return Undefined if wrong value is used', async () => {
      const testUserLogin: UserLoginDTO = {
        username: 'test',
        password: 'testpass',
      };
      const findEmailInTheDatabaseSpy = jest
        .spyOn(mockUserService, 'validateUserPassword')
        .mockImplementation(() => Promise.resolve())
      const result = await service.validateUserPassword(testUserLogin)
      expect(findEmailInTheDatabaseSpy).toBeCalledTimes(1)
      expect(result).toBeUndefined()
      findEmailInTheDatabaseSpy.mockRestore()
    })
    it('validateUserPassword should return correct value', async () => {
      const testUserLogin: UserLoginDTO = {
        username: 'test',
        password: 'testpass',
      };
      const findEmailInTheDatabaseSpy = jest
        .spyOn(mockUserService, 'validateUserPassword')
        .mockImplementation(() => Promise.resolve(testUserLogin))
      const result = await service.validateUserPassword(testUserLogin)
      expect(findEmailInTheDatabaseSpy).toBeCalledTimes(1)
      expect(result).toEqual(testUserLogin)
      findEmailInTheDatabaseSpy.mockRestore()
    })
  })
  describe('validateUserFromPayload', () => {
    it('validateUserFromPayload should call usersService.validateUserFromPayload with correct value', async () => {
      const mockPayload: JwtPayload = {
        username: 'test',
        password: 'TeSt'
      }
      const validateUserFromPayloadSpy = jest
        .spyOn(mockUserService, 'validateUserFromPayload')
        .mockImplementation(() => Promise.resolve(mockPayload))
      await service.validateUserFromPayload(mockPayload)
      expect(validateUserFromPayloadSpy).toBeCalledTimes(1)
      expect(validateUserFromPayloadSpy).toBeCalledWith(mockPayload)
      validateUserFromPayloadSpy.mockRestore()
    })
    it('validateUserFromPayload should return Undefined if wrong value is used ', async () => {
      const mockPayload: JwtPayload = {
        username: 'test',
        password: 'TeSt'
      }
      const validateUserFromPayloadSpy = jest
        .spyOn(mockUserService, 'validateUserFromPayload')
        .mockImplementation(() => Promise.resolve())
      const result = await service.validateUserFromPayload(mockPayload)
      expect(validateUserFromPayloadSpy).toBeCalledTimes(1)
      expect(result).toBeUndefined()
      validateUserFromPayloadSpy.mockRestore()
    })
    it('validateUserFromPayload should return correct value', async () => {
      const mockPayload: JwtPayload = {
        username: 'test',
        password: 'TeSt'
      }
      const validateUserFromPayloadSpy = jest
        .spyOn(mockUserService, 'validateUserFromPayload')
        .mockImplementation(() => Promise.resolve(mockPayload))
      const result = await service.validateUserFromPayload(mockPayload)
      expect(validateUserFromPayloadSpy).toBeCalledTimes(1)
      expect(result).toEqual(mockPayload)
      validateUserFromPayloadSpy.mockRestore()
    })
  })
  describe("isTokenBlacklisted", () => {
    it('isTokenBlacklisted should call blacklistService.checkIfBlacklisted with correct value', async () => {
      const mockToken = 'token';
      const isTokenBlacklistedSpy = jest
        .spyOn(mockBlacklistService, "checkIfBlacklisted")
        .mockImplementation(() => Promise.resolve(mockToken))
      await service.isTokenBlacklisted(mockToken)
      expect(isTokenBlacklistedSpy).toBeCalledTimes(1)
      expect(isTokenBlacklistedSpy).toBeCalledWith(mockToken)
    })
    it('isTokenBlacklisted should return correct value', async () => {
      const mockToken = 'token';
      const isTokenBlacklistedSpy = jest
        .spyOn(mockBlacklistService, "checkIfBlacklisted")
        .mockImplementation(() => Promise.resolve(mockToken))
      const result = await service.isTokenBlacklisted(mockToken)
      expect(isTokenBlacklistedSpy).toBeCalled()
      expect(result).toEqual(mockToken)
    })
  })
  describe('checkForRolesAndCreateThem', () => {
    it('checkForRolesAndCreateThem should call usersService.checkForRolesAndCreateThem', async () => {
      const checkForRolesAndCreateThemSpy = jest
        .spyOn(mockUserService, 'checkForRolesAndCreateThem')
        .mockImplementation(() => Promise.resolve())
      await service.checkForRolesAndCreateThem()
      expect(checkForRolesAndCreateThemSpy).toBeCalled()
      checkForRolesAndCreateThemSpy.mockRestore()
    })
  })
});
