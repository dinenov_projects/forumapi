import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { async } from 'rxjs/internal/scheduler/async';
import { UserLoginDTO } from '../models/user/user-login.dto';
import { NotFoundException, BadRequestException } from '@nestjs/common';
import { UserShowDTO } from '../models/user/user-show.dto';
import { UserRegisterDTO } from '../models/user/user-register.dto';

describe('Auth Controller', () => {
  let controller: AuthController;

  const mockAuthService = {
    findUserByUsername() { },
    validateUserPassword() { },
    login() { },
    logout() { },
    checkForRolesAndCreateThem() { },
    findEmailInTheDatabase() { },
    register() { }
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: AuthService,
          useValue: mockAuthService,
        },
      ],
      controllers: [AuthController],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('login', () => {
    it('should call authService.findUserByUsername with correct value', async () => {

      const testUserLogin: UserLoginDTO = {
        username: 'test',
        password: 'testpass',
      };
      const testUser: UserShowDTO = {
        id: '1',
        username: 'test',
        banstatus: {
          isBanned: false,
          description: 'This user is not banned!',
        },
        roles: [{ name: 'User' }],
      };
      const findUserByUsernameSpy = jest
        .spyOn(mockAuthService, 'findUserByUsername')
        .mockImplementation(() => Promise.resolve(testUser));

      const validateUserPasswordSpy = jest
        .spyOn(mockAuthService, 'validateUserPassword')
        .mockImplementation(() => Promise.resolve(testUser));

      const loginSpy = jest
        .spyOn(mockAuthService, 'login')
        .mockImplementation(() => Promise.resolve(testUser));

      await controller.login(testUserLogin);


      expect(findUserByUsernameSpy).toBeCalledTimes(1);
      expect(findUserByUsernameSpy).toBeCalledWith(testUserLogin.username);

      validateUserPasswordSpy.mockRestore();
      findUserByUsernameSpy.mockRestore();
      loginSpy.mockRestore();
    });
    it('should call authService.validateUserPassword with correct value', async () => {

      const testUserLogin: UserLoginDTO = {
        username: 'test',
        password: 'testpass',
      };
      const testUser: UserShowDTO = {
        id: '1',
        username: 'test',
        banstatus: {
          isBanned: false,
          description: 'This user is not banned!',
        },
        roles: [{ name: 'User' }],
      };
      const findUserByUsernameSpy = jest
        .spyOn(mockAuthService, 'findUserByUsername')
        .mockImplementation(() => Promise.resolve(testUser));

      const validateUserPasswordSpy = jest
        .spyOn(mockAuthService, 'validateUserPassword')
        .mockImplementation(() => Promise.resolve(testUser));

      const loginSpy = jest
        .spyOn(mockAuthService, 'login')
        .mockImplementation(() => Promise.resolve(testUser));

      await controller.login(testUserLogin);

      expect(validateUserPasswordSpy).toBeCalled();
      expect(validateUserPasswordSpy).toBeCalledWith(testUserLogin);
      validateUserPasswordSpy.mockRestore();
      findUserByUsernameSpy.mockRestore();
      loginSpy.mockRestore();

    });
    it('should throw error if incorrect parameters are given', async () => {
      try {
        const testUserLogin: UserLoginDTO = {
          username: 'test',
          password: 'testpass',
        };
        const validateUserPasswordSpy = jest
          .spyOn(mockAuthService, 'validateUserPassword')
          .mockImplementation(() => Promise.resolve());
        await controller.login(testUserLogin);
        expect(validateUserPasswordSpy).toEqual('test');
        validateUserPasswordSpy.mockRestore();
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
      }
    });
    it('should return correct parameters', async () => {

      const testUserLogin: UserLoginDTO = {
        username: 'test',
        password: 'testpass',
      };
      const testUser: UserShowDTO = {
        id: '1',
        username: 'test',
        banstatus: {
          isBanned: false,
          description: 'This user is not banned!',
        },
        roles: [{ name: 'User' }],
      };
      const findUserByUsernameSpy = jest
        .spyOn(mockAuthService, 'findUserByUsername')
        .mockImplementation(() => Promise.resolve(testUser));

      const validateUserPasswordSpy = jest
        .spyOn(mockAuthService, 'validateUserPassword')
        .mockImplementation(() => Promise.resolve(testUser));

      const loginSpy = jest
        .spyOn(mockAuthService, 'login')
        .mockImplementation(() => Promise.resolve(testUser));

      const result = await controller.login(testUserLogin);
      expect(result).toEqual({message: 'Successful login!', data: testUser});
      validateUserPasswordSpy.mockRestore();
      findUserByUsernameSpy.mockRestore();
      loginSpy.mockRestore();

    });
    it('should call authService.login with correct parameters', async () => {

      const testUserLogin: UserLoginDTO = {
        username: 'test',
        password: 'testpass',
      };
      const testUser: UserShowDTO = {
        id: '1',
        username: 'test',
        banstatus: {
          isBanned: false,
          description: 'This user is not banned!',
        },
        roles: [{ name: 'User' }],
      };
      const findUserByUsernameSpy = jest
        .spyOn(mockAuthService, 'findUserByUsername')
        .mockImplementation(() => Promise.resolve(testUser));

      const validateUserPasswordSpy = jest
        .spyOn(mockAuthService, 'validateUserPassword')
        .mockImplementation(() => Promise.resolve(testUser));

      const loginSpy = jest
        .spyOn(mockAuthService, 'login')
        .mockImplementation(() => Promise.resolve(testUser));

      await controller.login(testUserLogin);

      expect(loginSpy).toBeCalled();
      validateUserPasswordSpy.mockRestore();
      findUserByUsernameSpy.mockRestore();
      loginSpy.mockRestore();

    });
    describe('logout', () => {
      it("should call authService.logout with correct parameters", async () => {
        const mockToken = "token";
        const logoutSpy = jest
          .spyOn(mockAuthService, 'logout')
          .mockImplementation(() => Promise.resolve(mockToken));
        await controller.logout(mockToken);
        expect(logoutSpy).toBeCalled();
        expect(logoutSpy).toBeCalledWith(mockToken);
        logoutSpy.mockRestore();
      });

      it("should return correct value", async () => {
        const mockToken = "token";
        const logoutSpy = jest
          .spyOn(mockAuthService, 'logout')
          .mockImplementation(() => Promise.resolve(mockToken));
        const result = await controller.logout(mockToken);
        expect(logoutSpy).toBeCalled();
        expect(result).toEqual({message: 'Successful logout!', data: mockToken});
        logoutSpy.mockRestore();
      });
      it("should give undefined if incorect value is given", async () => {
        const mockToken = "token";
        const logoutSpy = jest
          .spyOn(mockAuthService, 'logout')
          .mockImplementation(() => Promise.resolve());
        const result = await controller.logout(mockToken);
        expect(logoutSpy).toBeCalled();
        expect(result).toBeUndefined();
        logoutSpy.mockRestore();
      });
    });
    describe('register', () => {
      it('should call authService.findUserByUsername with corect values', async () => {

        const mockUserRegisterDTO: UserRegisterDTO = {
          username: 'test',
          password: 'TeStPaSS',
          email: 'test@test.com'
        };
        const testUser: UserShowDTO = {
          id: '1',
          username: 'test',
          banstatus: {
            isBanned: false,
            description: 'This user is not banned!',
          },
          roles: [{ name: 'User' }],
        };
        const returnUser = { email: 'test@test.com' };

        const registerSpy = jest
          .spyOn(mockAuthService, 'register')
          .mockImplementation(() => Promise.resolve(returnUser));

        const findEmailInTheDatabaseSpy = jest
          .spyOn(mockAuthService, 'findEmailInTheDatabase')
          .mockImplementation(() => Promise.resolve(returnUser));

        const checkForRolesAndCreateThemSpy = jest
          .spyOn(mockAuthService, 'checkForRolesAndCreateThem')
          .mockImplementation(() => Promise.resolve());

        const findUserByUsernameSpy = jest
          .spyOn(mockAuthService, 'findUserByUsername')
          .mockImplementation(() => Promise.resolve(testUser));

        try {

          await controller.register(mockUserRegisterDTO);
        } catch (err) { }

        expect(findUserByUsernameSpy).toBeCalledWith(mockUserRegisterDTO.username);
        findUserByUsernameSpy.mockRestore();
        checkForRolesAndCreateThemSpy.mockRestore();
        findEmailInTheDatabaseSpy.mockRestore();
        registerSpy.mockRestore();
      });
      it('should throw error if authService.findUserByUsername is called with incorect values', async () => {
        try {
          const mockUserRegisterDTO: UserRegisterDTO = {
            username: 'test',
            password: 'TeStPaSS',
            email: 'test@test.com'
          };
          const testUser: UserShowDTO = {
            id: '1',
            username: 'test',
            banstatus: {
              isBanned: false,
              description: 'This user is not banned!',
            },
            roles: [{ name: 'User' }],
          };
          const findUserByUsernameSpy = jest
            .spyOn(mockAuthService, 'findUserByUsername')
            .mockImplementation(() => Promise.resolve());
          await controller.register(mockUserRegisterDTO);
          expect(findUserByUsernameSpy).toBeCalled();
          findUserByUsernameSpy.mockRestore();
        } catch (err) {
          expect(err).toBeInstanceOf(BadRequestException);
        }
      });
      it('should call authService.checkForRolesAndCreateThem ', async () => {
        const testUser: UserShowDTO = {
          id: '1',
          username: 'test',
          banstatus: {
            isBanned: false,
            description: 'This user is not banned!',
          },
          roles: [{ name: 'User' }],
        };
        const mockUserRegisterDTO: UserRegisterDTO = {
          username: 'test',
          password: 'TeStPaSS',
          email: 'test@test.com'
        };
        const returnUser = { email: 'test@test.com' };
        const registerSpy = jest
          .spyOn(mockAuthService, 'register')
          .mockImplementation(() => Promise.resolve(returnUser));
        const findEmailInTheDatabaseSpy = jest
          .spyOn(mockAuthService, 'findEmailInTheDatabase')
          .mockImplementation(() => Promise.resolve(returnUser));
        const checkForRolesAndCreateThemSpy = jest
          .spyOn(mockAuthService, 'checkForRolesAndCreateThem')
          .mockImplementation(() => Promise.resolve());
        const findUserByUsernameSpy = jest
          .spyOn(mockAuthService, 'findUserByUsername')
          .mockImplementation(() => Promise.resolve(testUser));
        try {
          await controller.register(mockUserRegisterDTO);
        } catch (err) { }
        expect(checkForRolesAndCreateThemSpy).toBeCalled();

        findUserByUsernameSpy.mockRestore();
        checkForRolesAndCreateThemSpy.mockRestore();
        findEmailInTheDatabaseSpy.mockRestore();
        registerSpy.mockRestore();


      });
      it('should call authService.findEmailInTheDatabase with corect values', async () => {

        const mockUserRegisterDTO: UserRegisterDTO = {
          username: 'test',
          password: 'TeStPaSS',
          email: 'test@test.com'
        };
        const testUser: UserShowDTO = {
          id: '1',
          username: 'test',
          banstatus: {
            isBanned: false,
            description: 'This user is not banned!',
          },
          roles: [{ name: 'User' }],
        };
        const returnUser = { email: 'test@test.com' };

        const registerSpy = jest
          .spyOn(mockAuthService, 'register')
          .mockImplementation(() => Promise.resolve(testUser));
        const findEmailInTheDatabaseSpy = jest
          .spyOn(mockAuthService, 'findEmailInTheDatabase')
          .mockImplementation(() => Promise.resolve(returnUser));
        const checkForRolesAndCreateThemSpy = jest
          .spyOn(mockAuthService, 'checkForRolesAndCreateThem')
          .mockImplementation(() => Promise.resolve());
        const findUserByUsernameSpy = jest
          .spyOn(mockAuthService, 'findUserByUsername')
          .mockImplementation(() => Promise.resolve(undefined));
        try {
          await controller.register(mockUserRegisterDTO);
        } catch (err) { }
        expect(findEmailInTheDatabaseSpy).toBeCalled();
        expect(findEmailInTheDatabaseSpy).toBeCalledWith(mockUserRegisterDTO.email);
        findUserByUsernameSpy.mockRestore();
        checkForRolesAndCreateThemSpy.mockRestore();
        findEmailInTheDatabaseSpy.mockRestore();
        registerSpy.mockRestore();

      });
      it('should throw error if authService.findEmailInTheDatabase is called with incorect values', async () => {
        try {
          const mockUserRegisterDTO: UserRegisterDTO = {
            username: 'test',
            password: 'TeStPaSS',
            email: 'test@test.com'
          };
          const findEmailInTheDatabaseSpy = jest
            .spyOn(mockAuthService, 'findEmailInTheDatabase')
            .mockImplementation(() => Promise.resolve());
          await controller.register(mockUserRegisterDTO);
          expect(findEmailInTheDatabaseSpy).toBeCalled();
          findEmailInTheDatabaseSpy.mockRestore();
        } catch (err) {
          expect(err).toBeInstanceOf(BadRequestException);
        }
      });
    });
  });
});
