import {MigrationInterface, QueryRunner} from "typeorm";

export class new1560435494968 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `banstatus` (`id` int NOT NULL AUTO_INCREMENT, `isBanned` tinyint NOT NULL DEFAULT 0, `description` varchar(255) NOT NULL DEFAULT 'This user is not banned!', PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `blacklist` (`id` int NOT NULL AUTO_INCREMENT, `token` varchar(255) NOT NULL, `date` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `flag_post_entity` (`id` int NOT NULL AUTO_INCREMENT, `reason` varchar(255) NOT NULL, `userIdId` int NULL, `postIdId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `post` (`id` int NOT NULL AUTO_INCREMENT, `title` varchar(255) NOT NULL, `description` varchar(255) NOT NULL, `body` varchar(255) NOT NULL, `datePosted` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isLocked` tinyint NOT NULL DEFAULT 0, `isDeleted` tinyint NOT NULL DEFAULT 0, `dateModified` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `authorId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `role` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user` (`id` int NOT NULL AUTO_INCREMENT, `username` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `isDeleted` tinyint NOT NULL DEFAULT 0, `banstatusId` int NULL, UNIQUE INDEX `IDX_78a916df40e02a9deb1c4b75ed` (`username`), UNIQUE INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`), UNIQUE INDEX `REL_15de78a2ad5e6dbfe54c777d7c` (`banstatusId`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `comments` (`id` int NOT NULL AUTO_INCREMENT, `body` varchar(255) NOT NULL, `dateCreated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `dateModified` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `authorId` int NULL, `postId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `post_like_user` (`postId` int NOT NULL, `userId` int NOT NULL, INDEX `IDX_59787c5411e8ae50749116cfc6` (`postId`), INDEX `IDX_91414229ccd1917c7aae71c95b` (`userId`), PRIMARY KEY (`postId`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `post_dislike_user` (`postId` int NOT NULL, `userId` int NOT NULL, INDEX `IDX_1b48dc98ed49fb12740974f791` (`postId`), INDEX `IDX_3745a572fbb95008618690b03b` (`userId`), PRIMARY KEY (`postId`, `userId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user_roles_role` (`userId` int NOT NULL, `roleId` int NOT NULL, INDEX `IDX_5f9286e6c25594c6b88c108db7` (`userId`), INDEX `IDX_4be2f7adf862634f5f803d246b` (`roleId`), PRIMARY KEY (`userId`, `roleId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user_following_user` (`userId_1` int NOT NULL, `userId_2` int NOT NULL, INDEX `IDX_9691163a986dfb589a90dea3d5` (`userId_1`), INDEX `IDX_a89f5a432c1edcd03a3b655532` (`userId_2`), PRIMARY KEY (`userId_1`, `userId_2`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `flag_post_entity` ADD CONSTRAINT `FK_54fc7aa00f57e7f7c80766f24e7` FOREIGN KEY (`userIdId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `flag_post_entity` ADD CONSTRAINT `FK_8d58e7d524adc7b066cf284b6ec` FOREIGN KEY (`postIdId`) REFERENCES `post`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `post` ADD CONSTRAINT `FK_c6fb082a3114f35d0cc27c518e0` FOREIGN KEY (`authorId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_15de78a2ad5e6dbfe54c777d7c7` FOREIGN KEY (`banstatusId`) REFERENCES `banstatus`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comments` ADD CONSTRAINT `FK_4548cc4a409b8651ec75f70e280` FOREIGN KEY (`authorId`) REFERENCES `user`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comments` ADD CONSTRAINT `FK_e44ddaaa6d058cb4092f83ad61f` FOREIGN KEY (`postId`) REFERENCES `post`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `post_like_user` ADD CONSTRAINT `FK_59787c5411e8ae50749116cfc69` FOREIGN KEY (`postId`) REFERENCES `post`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `post_like_user` ADD CONSTRAINT `FK_91414229ccd1917c7aae71c95bf` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `post_dislike_user` ADD CONSTRAINT `FK_1b48dc98ed49fb12740974f7913` FOREIGN KEY (`postId`) REFERENCES `post`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `post_dislike_user` ADD CONSTRAINT `FK_3745a572fbb95008618690b03ba` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_roles_role` ADD CONSTRAINT `FK_5f9286e6c25594c6b88c108db77` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_roles_role` ADD CONSTRAINT `FK_4be2f7adf862634f5f803d246b8` FOREIGN KEY (`roleId`) REFERENCES `role`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_following_user` ADD CONSTRAINT `FK_9691163a986dfb589a90dea3d5f` FOREIGN KEY (`userId_1`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `user_following_user` ADD CONSTRAINT `FK_a89f5a432c1edcd03a3b6555321` FOREIGN KEY (`userId_2`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user_following_user` DROP FOREIGN KEY `FK_a89f5a432c1edcd03a3b6555321`");
        await queryRunner.query("ALTER TABLE `user_following_user` DROP FOREIGN KEY `FK_9691163a986dfb589a90dea3d5f`");
        await queryRunner.query("ALTER TABLE `user_roles_role` DROP FOREIGN KEY `FK_4be2f7adf862634f5f803d246b8`");
        await queryRunner.query("ALTER TABLE `user_roles_role` DROP FOREIGN KEY `FK_5f9286e6c25594c6b88c108db77`");
        await queryRunner.query("ALTER TABLE `post_dislike_user` DROP FOREIGN KEY `FK_3745a572fbb95008618690b03ba`");
        await queryRunner.query("ALTER TABLE `post_dislike_user` DROP FOREIGN KEY `FK_1b48dc98ed49fb12740974f7913`");
        await queryRunner.query("ALTER TABLE `post_like_user` DROP FOREIGN KEY `FK_91414229ccd1917c7aae71c95bf`");
        await queryRunner.query("ALTER TABLE `post_like_user` DROP FOREIGN KEY `FK_59787c5411e8ae50749116cfc69`");
        await queryRunner.query("ALTER TABLE `comments` DROP FOREIGN KEY `FK_e44ddaaa6d058cb4092f83ad61f`");
        await queryRunner.query("ALTER TABLE `comments` DROP FOREIGN KEY `FK_4548cc4a409b8651ec75f70e280`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_15de78a2ad5e6dbfe54c777d7c7`");
        await queryRunner.query("ALTER TABLE `post` DROP FOREIGN KEY `FK_c6fb082a3114f35d0cc27c518e0`");
        await queryRunner.query("ALTER TABLE `flag_post_entity` DROP FOREIGN KEY `FK_8d58e7d524adc7b066cf284b6ec`");
        await queryRunner.query("ALTER TABLE `flag_post_entity` DROP FOREIGN KEY `FK_54fc7aa00f57e7f7c80766f24e7`");
        await queryRunner.query("DROP INDEX `IDX_a89f5a432c1edcd03a3b655532` ON `user_following_user`");
        await queryRunner.query("DROP INDEX `IDX_9691163a986dfb589a90dea3d5` ON `user_following_user`");
        await queryRunner.query("DROP TABLE `user_following_user`");
        await queryRunner.query("DROP INDEX `IDX_4be2f7adf862634f5f803d246b` ON `user_roles_role`");
        await queryRunner.query("DROP INDEX `IDX_5f9286e6c25594c6b88c108db7` ON `user_roles_role`");
        await queryRunner.query("DROP TABLE `user_roles_role`");
        await queryRunner.query("DROP INDEX `IDX_3745a572fbb95008618690b03b` ON `post_dislike_user`");
        await queryRunner.query("DROP INDEX `IDX_1b48dc98ed49fb12740974f791` ON `post_dislike_user`");
        await queryRunner.query("DROP TABLE `post_dislike_user`");
        await queryRunner.query("DROP INDEX `IDX_91414229ccd1917c7aae71c95b` ON `post_like_user`");
        await queryRunner.query("DROP INDEX `IDX_59787c5411e8ae50749116cfc6` ON `post_like_user`");
        await queryRunner.query("DROP TABLE `post_like_user`");
        await queryRunner.query("DROP TABLE `comments`");
        await queryRunner.query("DROP INDEX `REL_15de78a2ad5e6dbfe54c777d7c` ON `user`");
        await queryRunner.query("DROP INDEX `IDX_e12875dfb3b1d92d7d7c5377e2` ON `user`");
        await queryRunner.query("DROP INDEX `IDX_78a916df40e02a9deb1c4b75ed` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
        await queryRunner.query("DROP TABLE `role`");
        await queryRunner.query("DROP TABLE `post`");
        await queryRunner.query("DROP TABLE `flag_post_entity`");
        await queryRunner.query("DROP TABLE `blacklist`");
        await queryRunner.query("DROP TABLE `banstatus`");
    }

}
