import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, OneToMany, OneToOne, JoinColumn } from 'typeorm';
import { Post } from './post.entity';
import { Comments } from './comment.entity';
import { Banstatus } from './banstatus.entity';
import { Role } from './role.entity';
import { FlagPostEntity } from './flag.entity';
@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: string;
    @Column({unique: true})
    username: string;
    @Column('nvarchar')
    password: string;
    @Column({unique: true})
    email: string;
    @JoinTable()
    @ManyToMany(type => Role, { eager: true })
    roles: Role[];
    @OneToMany(type => Post, post => post.author)
    posts: Promise<Post[]>;
    @OneToMany(type => Comments, comment => comment.author)
    comments: Promise<Comments[]>;
    @JoinTable()
    @ManyToMany(type => User, user => user.followers)
    following: Promise<User[]>;
    @ManyToMany(type => User, user => user.following)
    followers: Promise<User[]>;
    @Column('boolean', {default: false})
    isDeleted: boolean;
    @JoinColumn()
    @OneToOne(type => Banstatus, {eager: true})
    banstatus: Banstatus;
    @ManyToMany(type => Post, post => post.like)
    likes: Promise<Post[]>;
    @ManyToMany(type => Post, post => post.dislike)
    dislikes: Promise<Post[]>;
    @OneToMany(type => FlagPostEntity, flag => flag.userId)
    flag: Promise<FlagPostEntity[]>;
}
