import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, CreateDateColumn, OneToMany, JoinTable, ManyToMany, AfterUpdate } from 'typeorm';
import { User } from './user.entity';
import { Comments } from './comment.entity';
import { FlagPostEntity } from './flag.entity';

@Entity()
export class Post {
    @PrimaryGeneratedColumn()
    id: string;
    @Column('nvarchar')
    title: string;
    @Column('nvarchar')
    description: string;
    @Column('nvarchar')
    body: string;
    @OneToMany(type => Comments, comments => comments.post)
    comments: Promise<Comments[]>;
    @ManyToOne(type => User, user => user.posts)
    author: Promise<User>;
    @CreateDateColumn()
    datePosted: Date;
    @Column('boolean', { default: false })
    isLocked: boolean;
    @Column('boolean', { default: false })
    isDeleted: boolean;
    @CreateDateColumn()
    dateModified: Date;
    @ManyToMany(type => User, user => user.likes)
    @JoinTable()
    like: Promise<User[]>;
    @ManyToMany(type => User, user => user.dislikes)
    @JoinTable()
    dislike: Promise<User[]>;
    @OneToMany(type => FlagPostEntity, flag => flag.postId, { eager: true })
    @JoinTable()
    flag: FlagPostEntity[];
    @AfterUpdate()
    updateDate() {
        this.dateModified = new Date();
    }
}
