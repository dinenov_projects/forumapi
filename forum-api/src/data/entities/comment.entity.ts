import { Entity, Column, ManyToOne, CreateDateColumn, PrimaryGeneratedColumn, AfterUpdate } from 'typeorm';
import { User } from './user.entity';
import { Post } from './post.entity';

@Entity()
export class Comments {
    @PrimaryGeneratedColumn()
    id: string;
    @Column('nvarchar')
    body: string;
    @ManyToOne(type => User, user => user.comments)
    author: Promise<User>;
    @ManyToOne(type => Post, post => post.comments)
    post: Promise<Post>;
    @CreateDateColumn()
    dateCreated: Date;
    @CreateDateColumn()
    dateModified: Date;
    @Column('boolean', { default: false })
    isDeleted: boolean;
    @AfterUpdate()
    updateDate() {
        this.dateModified = new Date();
    }
}
