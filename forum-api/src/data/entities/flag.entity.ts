import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Post } from './post.entity';
import { User } from './user.entity';

@Entity()
export class FlagPostEntity {
    @PrimaryGeneratedColumn()
    id: string;
    @Column()
    reason: string;
    @ManyToOne(type => User, user => user.flag)
    userId: Promise<User>;
    @ManyToOne(type => Post, post => post.flag)
    postId: Post;
}
