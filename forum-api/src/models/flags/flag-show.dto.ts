import { Expose, Exclude } from 'class-transformer';
import { IsString } from 'class-validator';

@Exclude()
export class FlagShowDTO {
    @Expose()
    @IsString()
    reason: string;
}
