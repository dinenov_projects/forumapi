import {  IsString } from 'class-validator';

export class FlagPostDTO {
    @IsString()
    reason: string;
}
