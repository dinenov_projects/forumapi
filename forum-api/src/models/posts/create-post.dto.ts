import { IsString, Length } from 'class-validator';

export class CreatePostDTO {
    @IsString()
    @Length(1, 200)
    readonly title: string;
    @IsString()
    @Length(0, 250)
    readonly description: string;
    @IsString()
    @Length(1, 1000)
    readonly body: string;
}
