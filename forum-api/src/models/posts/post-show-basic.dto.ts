import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class PostShowBasicDTO {
    @Expose()
    id: string;
    @Expose()
    title: string;
    @Expose()
    description: string;
    @Expose()
    datePosted: Date;
    @Expose()
    dateModified: Date;
}
