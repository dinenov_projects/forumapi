import { Exclude, Expose, Type } from 'class-transformer';
import { IsString, IsDefined, IsDate, IsBoolean } from 'class-validator';
import { UserShowDTO } from '../user';
import { FlagShowDTO } from '../flags/flag-show.dto';
import { ShowCommentDTO } from '../comments/show-comment.dto';

@Exclude()
export class PostShowWithoutCommentsDTO {
    @Expose()
    @IsString()
    id: string;
    @Expose()
    @IsString()
    title: string;
    @Expose()
    @IsString()
    description: string;
    @Expose()
    @IsString()
    body: string;
    @Expose()
    @IsDefined()
    @Type(() => UserShowDTO)
    author: UserShowDTO;
    @Expose()
    @IsDate()
    datePosted: Date;
    @Expose()
    @IsDate()
    dateModified: Date;
    @Expose()
    @IsBoolean()
    isLocked: boolean;
    @Expose()
    @Type(() => UserShowDTO)
    like: UserShowDTO[];
    @Expose()
    @Type(() => UserShowDTO)
    dislike: UserShowDTO[];
    @Expose()
    @Type(() => FlagShowDTO)
    flag: FlagShowDTO[];
}
