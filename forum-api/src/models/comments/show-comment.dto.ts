import { Exclude, Expose, Type } from 'class-transformer';
import { UserShowDTO } from '../user';

@Exclude()
export class ShowCommentDTO {
    @Expose()
    id: string;
    @Expose()
    body: string;
    @Expose()
    @Type(() => UserShowDTO)
    author: UserShowDTO;
    @Expose()
    dateCreated: Date;
    @Expose()
    dateModified: Date;
}
