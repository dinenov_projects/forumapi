import 'class-transformer';
import { Expose, Exclude, Type } from 'class-transformer';
import { ShowCommentDTO } from '../comments/show-comment.dto';
import { PostShowBasicDTO } from '../posts/post-show-basic.dto';

@Exclude()
export class ActivityShowDTO {
    @Expose()
    id: string;
    @Expose()
    username: string;
    @Expose()
    @Type(() => PostShowBasicDTO)
    posts: PostShowBasicDTO[];
    @Expose()
    @Type(() => ShowCommentDTO)
    comments: ShowCommentDTO[];
    @Expose()
    @Type(() => PostShowBasicDTO)
    likes: PostShowBasicDTO[];
    @Expose()
    @Type(() => PostShowBasicDTO)
    dislikes: PostShowBasicDTO[];
}
