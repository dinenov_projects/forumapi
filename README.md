## Forum API

  

We created a Backend Forum API to test our knowledge of NEST JS and DB.


  
## Trello board



https://trello.com/b/z0rEfYDk



## Installation

  

```bash

$ npm install

```

  

## Running the app

  

```bash

# development

$ npm run start

  

# watch mode

$ npm run start:dev

  

# production mode

$ npm run start:prod

```

  

## Test

  

```bash

# unit tests

$ npm run test

  

# e2e tests

$ npm run test:e2e

  

# test coverage

$ npm run test:cov

```

  

## Support

  

This is a one off project no continues support wath so ever.

  

## Built With

NEST, MariaDB, SQL Workbench, Blood, Sweat and Tears...

## Authors

#### Dimitar Nenov,

  

#### Martin Nedyalkov,

  

#### Mihail Mishev

# Usage Paths

  

## For users:

#### ``POST "/users"`` = Will give you user login.
Request body:

```bash
{
"username": string,
"password": string,
"email": string,
}
```

#### ``POST "/session"`` = Will Validate user.
Request body:

```bash
{
"username": string,
"password": string
}
```

#### ``DELETE "/session"`` = Will terminate user token.

  

## For Posts:

#### ``GET "/posts"``  = Will retrieve all posts.

#### ``GET "/users/:userId/posts"`` = Will retrieve all post of specific user.

#### ``GET "/users/posts/sort" `` = Will sort post by query parameter.

#### ``POST "/posts"`` = Will create a new Post.
Request body:

```bash
{
"title": string,
"description": string,
"body": string
}
```

#### ``GET "/posts/:postId"`` =Will return post by ID.

#### `` PUT "/posts/:postId"`` =Used for updating posts.
Request body:
```bash
{
"title": string,
"description": string,
"body": string
}
```

#### ``DELETE "/posts/:postId"`` = Deletes post by ID.

#### ``PUT "/posts/:postId/lock"`` = Locks post from further change.

#### ``PUT "posts/:postId/flag" `` = Flags post for inappropriate behavior.
Request body:
```bash
{
"reason": string
}
```

#### ``PUT "/posts/:postId/likes" `` = Add Like to post.

#### ``Put "posts/:postId/dislikes" `` = Add Dislike to post.

  

## For Comments:

#### ``POST "posts/:postId/comments"`` = Adds comment to post.
Request body:
```bash
{
"body": string
}
```

#### ``PUT "posts/:postId/comments/:commentId"`` = Updates comment.
Request body:
```bash
{
"body": string
}
```

#### ``DELETE "posts/:postId/comments/:commentId"`` = Deletes comment.

#### ``GET "posts/:postId/comments/:commentId"`` = Retrieves comment by ID.

  

## For Admin:

#### ``DELETE "users/:userId" `` = Deletes user.

#### ``PUT "users/:userId/banstatus" `` = Banns user.

#### ``DELETE"users/:userId/banstatus"``  = Unbanns user.

## For User/Friends:

#### ``GET "users/friends"`` = Retrieve logged user following and followers information.

#### ``GET "users/:userId/activity"``  = See user activity.

#### ``POST "users/:userId/follow" `` = Follow user.

#### ``DELETE "users/:userId/follow" `` = Unfollow user.

  

## Acknowledgments

#### Thanks to Rosen Urkov and Stoyan (Lastname unknown maybe Targaryen:D) for all the help.
